package com.viddmonk.service;

import static com.googlecode.javacv.cpp.opencv_core.cvCopy;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

import com.googlecode.javacv.FFmpegFrameGrabber;
import com.googlecode.javacv.FFmpegFrameRecorder;
import com.googlecode.javacv.Frame;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.avcodec;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.IplROI;

public class TestWatermark {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			IplROI roi = new IplROI();
			roi.xOffset(385);
			roi.yOffset(270);
			roi.width(30);
			roi.height(30);
			IplImage logoImg = cvLoadImage("/Users/rvadlam/Documents/VideoFiles/vdMonkLogo.png");
			FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VID4.mp4");
			grabber1.start();
			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder("/Users/rvadlam/Documents/VideoFiles/ConcatVID4_watermark.mp4", grabber1.getImageWidth(), grabber1.getImageHeight(), grabber1.getAudioChannels());
	        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
	        recorder.setFormat("mp4");
			recorder.start();
			Frame frame;
			while (true) {			
				frame = grabber1.grabFrame();
				if( frame == null ) {
					break;
				}
				if( !frame.keyFrame ) {
					IplImage image = frame.image;
					if( image != null ) {
						IplImage image2 = image.roi(roi);
						cvCopy(logoImg,image2,null);					
					}					
				    try {
						recorder.record(image);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}					
				} else {
				    try {
						recorder.record(frame);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}										
				}
			}
			recorder.stop();
			grabber1.stop();
		} catch (Exception e) {
			e.printStackTrace();
		} catch (com.googlecode.javacv.FrameRecorder.Exception e) {
			e.printStackTrace();
		} 

	}
}
