package com.viddmonk.service;

import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class TestImageGrab {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//        final OpenCVFrameGrabber grabber = new OpenCVFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VID4.mp4");
		File file = new File("https://s3.amazonaws.com/viddmonk/VIDEO/E_1392482673346Wildlife_512kb.mp4");
      final OpenCVFrameGrabber grabber2 = new OpenCVFrameGrabber(file);
      try {
    	  		URL url = new URL("https://s3.amazonaws.com/viddmonk/VIDEO/E_1392482673346Wildlife_512kb.mp4");
    	  		File myfile = Loader.extractResource(url, null, null, null);
    	        final OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(myfile);
	            grabber.start();
	            System.out.println("Frame Lenght: " + grabber.getLengthInFrames());            
	            System.out.println("Frame Rate: " + grabber.getFrameRate());            
	            System.out.println("Image Height: " + grabber.getImageHeight());            
	            System.out.println("Image Width: " + grabber.getImageWidth());            
	            long frameCount = 0;
	            IplImage img = grabber.grab();
	            if(img != null) {
	                cvSaveImage("/Users/rvadlam/Documents/VideoFiles/capture.jpg", img);
	            }
	            grabber.setFrameNumber(160);
	            IplImage img2 = grabber.grab();
	            if(img2 != null) {
	                cvSaveImage("/Users/rvadlam/Documents/VideoFiles/capture2.jpg", img2);
	            }
	            System.out.println("Frame Count: " + frameCount);

	        } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
	            // Close the video file
	            try {
					grabber2.release();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	}
}