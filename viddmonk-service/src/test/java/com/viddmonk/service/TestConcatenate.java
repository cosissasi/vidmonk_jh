package com.viddmonk.service;

import com.googlecode.javacv.FFmpegFrameGrabber;
import com.googlecode.javacv.FFmpegFrameRecorder;
import com.googlecode.javacv.Frame;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.avcodec;

public class TestConcatenate {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VID4.mp4");
			grabber1.start();
			FFmpegFrameGrabber grabber2 = new FFmpegFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VIDOriginal.mp4");
			grabber2.start();
			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(
					"/Users/rvadlam/Documents/VideoFiles/PostVID.mp4",
					grabber1.getImageWidth(), grabber1.getImageHeight(),
					grabber1.getAudioChannels());
	        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
	        recorder.setFormat("mp4");
			recorder.start();
			Frame frame;
			int frameCount = 0;
			grabber1.setFrameNumber(++frameCount);
			while (true) {			
				frame = grabber1.grabFrame();
				if( frame == null ) {
					break;
				}
			    try {
						recorder.record(frame);			    		
				} catch (java.lang.Exception e) {
					e.printStackTrace();
				}					
			}
			frameCount = 0;
			grabber2.setFrameNumber(++frameCount);
			while (true) {
				frame = grabber2.grabFrame();
				if( frame == null ) {
					break;
				}
				recorder.setTimestamp(grabber2.getTimestamp());
			    try {
			    	recorder.record(frame);
				} catch (java.lang.Exception e) {
					e.printStackTrace();
				}					
			}
			recorder.stop();
			grabber2.stop(); 
			grabber1.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (com.googlecode.javacv.FrameRecorder.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}