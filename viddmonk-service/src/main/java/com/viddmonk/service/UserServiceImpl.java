/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.viddmonk.dao.UserDAO;
import com.viddmonk.domain.AccountState;
import com.viddmonk.domain.Role;
import com.viddmonk.domain.User;
import com.viddmonk.repository.RoleRepository;
import com.viddmonk.repository.UserRepository;

/**
 * User Service
 * @author rvadlam
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	/**
	 * find by username
	 * @param	username	Username
	 * @return	User
	 * 
	 */
	public User findByUsername(String username) {
		return userDAO.findByUsername(username);
	}
	
	/**
	 * update
	 * @param	name	Name
	 * @param	mobileNumber	Mobile Number
	 * @param	zipCode	Zip Code
	 * @param	smsNotification SMS Notification
	 * @param	emailNotifiction Email Notification
	 * 
	 */
	public User update(String username, String name, String mobileNumber, String zipCode, Boolean smsNotification, Boolean emailNotification) {
		User user = userDAO.findByUsername(username);
		if( !StringUtils.isEmpty(name)) {
			user.setName(name);
		}
		if( !StringUtils.isEmpty(mobileNumber)) {
			user.setMobileNumber(mobileNumber);
		}
		if( !StringUtils.isEmpty(zipCode)) {
			user.setZipCode(zipCode);
		}
		if( smsNotification != null ) {
			user.setSmsNotification(smsNotification);
		}
		if( emailNotification != null ) {
			user.setEmailNotification(emailNotification);
		}
		return userRepository.save(user);
	}
	
	/**
	 * register user
	 * @param	user	User
	 * @return	User
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public User registerUser(User user) {
		if( user != null ) {
			User retrievedUser = userRepository.findByUsername(user.getUsername());
			Role retrievedRole = roleRepository.findByRole("USER");
			if( retrievedUser == null ) {
				String uuid = UUID.randomUUID().toString();
				try {
					user.setPassword(sha256Password(uuid.substring(0, 10)));
				} catch (NoSuchAlgorithmException e) {
					return null;
				}
				user.setOneTimePassword(uuid.substring(0, 10));
				user.setAccountState(AccountState.UnVerfied);
				user.setRole(retrievedRole);
				return userRepository.save(user);							
			}
		}
		return null;
	}
	
	/**
	 * password reset
	 * @param	user	User
	 * @return	User
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public User passwordReset(User user) {
		if( user != null && user.getUsername() != null ) {
			User retrievedUser = userDAO.findByUsername(user.getUsername());
			if ( retrievedUser.getAccountState() == AccountState.UnVerfied ||  
			     retrievedUser.getAccountState() == AccountState.PasswordReset ||
			     retrievedUser.getAccountState() == AccountState.Locked ) {
					String uuid = UUID.randomUUID().toString();
					try {
						retrievedUser.setPassword(sha256Password(uuid.substring(0, 10)));
					} catch (NoSuchAlgorithmException e) {
						return null;
					}
					retrievedUser.setOneTimePassword(uuid.substring(0, 10));
					retrievedUser.setAccountState(AccountState.PasswordReset);
					retrievedUser.setNumberOfFailedAttempts(0);
					return userRepository.save(retrievedUser);
			}
		}
		return null;
	}
	
	/**
	 * change password
	 * @param	user	User
	 * @return	User
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public User changePassword(User user) {
		if( user != null && user.getUsername() != null ) {
			User retrievedUser = userDAO.findByUsername(user.getUsername());
			if ( retrievedUser.getAccountState() == AccountState.Active ) {
				retrievedUser.setAccountState(AccountState.Active);
				retrievedUser.setNumberOfFailedAttempts(0);
				retrievedUser.setPassword(user.getPassword());
				return userRepository.save(retrievedUser);
			}
		}
		return null;
	}
	
	/**
	 * create SHA-256 password
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private String sha256Password(String password) throws NoSuchAlgorithmException {	    
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());
 
        byte byteData[] = md.digest();
 
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
 
        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<byteData.length;i++) {
    		String hex=Integer.toHexString(0xff & byteData[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	System.out.println("Hex format : " + hexString.toString());
    	return hexString.toString();
	}
}