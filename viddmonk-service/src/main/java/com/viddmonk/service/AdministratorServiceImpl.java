/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.viddmonk.dao.AdministratorDAO;
import com.viddmonk.domain.AccountState;
import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.AdministratorRole;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.BrandAdministrator;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.CampaignAdministrator;
import com.viddmonk.domain.Media;
import com.viddmonk.domain.MediaType;
import com.viddmonk.repository.AdministratorRepository;
import com.viddmonk.repository.AdministratorRoleRepository;
import com.viddmonk.repository.BrandAdministratorRepository;
import com.viddmonk.repository.BrandRepository;
import com.viddmonk.repository.CampaignAdministratorRepository;
import com.viddmonk.repository.CampaignRepository;
import com.viddmonk.repository.MediaRepository;
import com.viddmonk.util.SendMail;

/**
 * Administrator Service
 * @author rvadlam
 *
 */
@Service("administratorService")
public class AdministratorServiceImpl implements AdministratorService {

	@Autowired
	private AdministratorDAO administratorDAO;

	@Autowired
	private AdministratorRepository administratorRepository;

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private BrandAdministratorRepository brandAdministratorRepository;

	@Autowired
	private CampaignAdministratorRepository campaignAdministratorRepository;

	@Autowired
	private AdministratorRoleRepository administratorRoleRepository;

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private MediaRepository mediaRepository;

	@Autowired
	private SendMail sendMail;
	
	private String AMAZON_BASE_URL = "https://s3.amazonaws.com/viddmonk";

	/**
	 * find by username
	 * @param	username	Username
	 * @return	User
	 * 
	 */
	public Administrator findByUsername(String username) {
		return administratorDAO.findByUsername(username);
	}
	
	/**
	 * register administrator
	 * @param	administrator	Administrator
	 * @return	Administrator
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public Administrator registerAdministrator(Administrator administrator) {
		if( administrator != null ) {
			AdministratorRole administratorRole = administratorRoleRepository.findByRole("ADMIN");
			if( administratorRole == null ) {
				administratorRole = new AdministratorRole();
				administratorRole.setRole("ADMIN");
				administratorRole.setDescription("Adminstrator Role");
				administratorRole = administratorRoleRepository.save(administratorRole);
			}
			Administrator retrievedAdministrator = administratorRepository.findByUsername(administrator.getUsername());
			if( retrievedAdministrator == null ) {
				String uuid = UUID.randomUUID().toString();
				try {
					administrator.setUsername(administrator.getEmail());
					administrator.setPassword(sha256Password(uuid.substring(0, 10)));
				} catch (NoSuchAlgorithmException e) {
					return null;
				}
				administrator.setAccountState(AccountState.UnVerfied);
				administrator.setRole(administratorRole);
				Administrator newAdministrator = administratorRepository.save(administrator);
				SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
				simpleMailMessage.setTo(newAdministrator.getEmail());
				simpleMailMessage.setSubject("Viddmonk Successful Registration Notification");
				simpleMailMessage.setText("Congratulations on your successful registration on Viddmonk site.\n\n Your one time password is : " + uuid.substring(0, 10) + " . Please use this password to login and change your password.");
				sendMail.sendMail(simpleMailMessage);
				return newAdministrator;
			}
		}
		return null;
	}
	
	/**
	 * save administrator
	 * @param	administrator	Administrator
	 * @return	Administrator
	 * 
	 */
	@Transactional
	public Administrator save(Administrator administrator) {
		return administratorRepository.save(administrator);
	}
	
	/**
	 * save brand by administrator
	 * @param	brand	Brand
	 * @param	username	User name
	 * @return	
	 * 
	 */
	@Transactional
	public Brand saveBrand(Brand brand, String username, MultipartFile imageFile) {
		if( imageFile != null ) {
			buildBrandMedia(brand, MediaType.IMAGE, imageFile);			
		}
		Brand newBrand = brandRepository.save(brand);
		Administrator administrator = administratorRepository.findByUsername(username);
		if( administrator != null ) {
			BrandAdministrator queriedBrandAdministrator = brandAdministratorRepository.findByBrandAndAdministrator(newBrand, administrator);
			if( queriedBrandAdministrator == null ) {
				BrandAdministrator brandAdministrator = new BrandAdministrator();
				brandAdministrator.setBrand(newBrand);
				brandAdministrator.setAdministrator(administrator);
				return brandAdministratorRepository.save(brandAdministrator).getBrand();				
			}
		}
		return null;
	}
	
	/**
	 * save campaign by administrator
	 * @param	campaign	Campaign
	 * @param	username	User name
	 * @return	
	 * 
	 */
	@Transactional
	public Campaign saveCampaign(Campaign campaign, String brandId,
			String username, List<MultipartFile> imageFiles,
			List<MultipartFile> audioFiles, List<MultipartFile> videoFiles) {
		String campaingId = campaign.getId();
		Brand brand = brandRepository.findById(brandId);
		if( brand != null ) {
			campaign.setBrand(brand);
			if( imageFiles != null && !imageFiles.isEmpty() ) {
				for(MultipartFile multipartFile : imageFiles) {
					buildMedia(campaign, MediaType.IMAGE, multipartFile);
				}
			}
			if( audioFiles != null && !audioFiles.isEmpty() ) {
				for(MultipartFile multipartFile : audioFiles) {
					buildMedia(campaign, MediaType.AUDIO, multipartFile);
				}
			}
			if( videoFiles != null && !videoFiles.isEmpty() ) {
				for(MultipartFile multipartFile : videoFiles) {
					buildMedia(campaign, MediaType.VIDEO, multipartFile);
				}
			}
			Campaign newCampaign = campaignRepository.save(campaign);
			Administrator administrator = administratorRepository.findByUsername(username);
			if( administrator != null && (campaingId == null || "".equals(campaingId)) ) {
				CampaignAdministrator campaignAdministrator = new CampaignAdministrator();
				campaignAdministrator.setCampaign(newCampaign);
				campaignAdministrator.setAdministrator(administrator);
				campaignAdministratorRepository.save(campaignAdministrator);
			}
			return newCampaign;
		}
		return null;
	}

	/**
	 * build media
	 * @param campaign	Campaign
	 * @param mediaType	MediaType
	 * 
	 */
	private void buildMedia(Campaign campaign, MediaType mediaType, MultipartFile multipartFile) {
		Media media = new Media();
		media.setFilename(multipartFile.getOriginalFilename());
		media.setFileSize(multipartFile.getSize());
		media.setMediaType(mediaType);
		media.setUploadedTime(new Date());
		long timestamp = Calendar.getInstance().getTimeInMillis();
		media.setS3URL(AMAZON_BASE_URL + "/" + mediaType.toString() + "/V_" + timestamp + media.getFilename());
		try {			 
			mediaRepository.save(media);
			if( mediaType != null && mediaType.equals(MediaType.IMAGE) ) {
				if (campaign.getX1() != null && campaign.getX2() != null
						&& campaign.getY1() != null && campaign.getY2() != null
						&& campaign.getW() != null && campaign.getH() != null) {
					BufferedImage image = ImageIO.read(multipartFile.getInputStream());
					BufferedImage cropped = image.getSubimage(new Integer(campaign.getX1()), new  Integer(campaign.getY1()), new Integer(campaign.getW()), new Integer(campaign.getH()));
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                String[] fileParts = media.getFilename().split("\\.");
	                String fileType = "png";
	                if( fileParts != null && fileParts.length > 0 ) {
	                	fileType = fileParts[fileParts.length - 1];
	                }
					ImageIO.write( cropped, fileType, baos );
					baos.flush();
					byte[] imageInByte = baos.toByteArray();
					baos.close();
					amazonService.putAsset(mediaType.toString(), "V_" + timestamp + media.getFilename(), new ByteArrayInputStream(imageInByte));					
				}	else {
					amazonService.putAsset(mediaType.toString(), "V_" + timestamp + media.getFilename(), new ByteArrayInputStream(multipartFile.getBytes()));					
				}
			} 
			campaign.getCampaignImageList().add(media);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * build media
	 * @param campaign	Campaign
	 * @param mediaType	MediaType
	 * 
	 */
	public void saveCroppedImage(Campaign campaign) {
		
		Media media = campaign.getCampaignImageList().get(0);
		try {			 
			if (campaign.getX1() != null && campaign.getX2() != null
					&& campaign.getY1() != null && campaign.getY2() != null
					&& campaign.getW() != null && campaign.getH() != null) {
				BufferedImage image = ImageIO.read(new URL(media.getS3URL()));
				BufferedImage cropped = image.getSubimage(new Integer(campaign.getX1()), new  Integer(campaign.getY1()), new Integer(campaign.getW()), new Integer(campaign.getH()));
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
                String[] fileParts = media.getFilename().split("\\.");
                String fileType = "png";
                if( fileParts != null && fileParts.length > 0 ) {
                	fileType = fileParts[fileParts.length - 1];
                }
				ImageIO.write( cropped, fileType, baos );
				baos.flush();
				byte[] imageInByte = baos.toByteArray();
				baos.close();
				String[] filename = media.getS3URL().split("V_");
				if( filename.length > 0 ) {
					amazonService.putAsset(media.getMediaType().toString(), "V_" + filename[filename.length-1], new ByteArrayInputStream(imageInByte));										
				} else {
					amazonService.putAsset(media.getMediaType().toString(), media.getS3URL(), new ByteArrayInputStream(imageInByte));					
				}
			}
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * build brand media
	 * @param brand	Brand
	 * @param mediaType	MediaType
	 * 
	 */
	private void buildBrandMedia(Brand brand, MediaType mediaType, MultipartFile multipartFile) {
		Media media = new Media();
		media.setFilename(multipartFile.getOriginalFilename());
		media.setFileSize(multipartFile.getSize());
		media.setMediaType(mediaType);
		media.setUploadedTime(new Date());
		long timestamp = Calendar.getInstance().getTimeInMillis();
		media.setS3URL(AMAZON_BASE_URL + "/" + mediaType.toString() + "/V_" + Calendar.getInstance().getTimeInMillis() + media.getFilename());
		try {			 
			amazonService.putAsset(mediaType.toString(), "V_" + timestamp + media.getFilename(), new ByteArrayInputStream(multipartFile.getBytes()));
			mediaRepository.save(media);
			brand.getImageList().add(media);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
		
	/**
	 * my brands
	 * @param	administrator	Administrator
	 * @return	List<Brand>
	 * 
	 */
	public List<Brand> myBrands(String email) {
		Administrator administrator = administratorRepository.findByEmail(email);
		List<BrandAdministrator> brandAdministratorList = brandAdministratorRepository.findByAdministrator(administrator);
		List<Brand> brands = new ArrayList<Brand>();
		for(BrandAdministrator brandAdministrator : brandAdministratorList) {
			brands.add(brandAdministrator.getBrand());
		}
		return brands;
	}	
	
	/**
	 * my campaigns
	 * @param	campaignAdministrator	campaignAdministrator
	 * @return	CampaignAdministrator
	 * 
	 */
	public Page<CampaignAdministrator> myCampaigns(String email, Pageable pageable) {
		Administrator administrator = administratorRepository.findByEmail(email);
		Page<CampaignAdministrator> campaignAdministratorList = campaignAdministratorRepository.findByAdministrator(administrator, pageable);
		return campaignAdministratorList;
	}
	
	/**
	 * password reset
	 * @param	administrator	Administrator
	 * @return	Administrator
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public Administrator passwordReset(Administrator administrator) {
		if( administrator != null && administrator.getUsername() != null ) {
			Administrator retrievedAdministrator = administratorDAO.findByUsername(administrator.getUsername());
			if ( retrievedAdministrator.getAccountState() == AccountState.UnVerfied ||  
					retrievedAdministrator.getAccountState() == AccountState.PasswordReset ||
							retrievedAdministrator.getAccountState() == AccountState.Locked ) {
					String uuid = UUID.randomUUID().toString();
					try {
						retrievedAdministrator.setPassword(sha256Password(uuid.substring(0, 10)));
					} catch (NoSuchAlgorithmException e) {
						return null;
					}
					retrievedAdministrator.setAccountState(AccountState.PasswordReset);
					retrievedAdministrator.setNumberOfFailedAttempts(0);
					Administrator newAdministrator =  administratorRepository.save(retrievedAdministrator);
					SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
					simpleMailMessage.setTo(newAdministrator.getEmail());
					simpleMailMessage.setSubject("Viddmonk Reset Password Notification");
					simpleMailMessage.setText("Your password has been reset on Viddmonk site.\n\n Your one time password is : " + uuid.substring(0, 10) + " . Please use this password to login and change your password.");
					sendMail.sendMail(simpleMailMessage);
					return newAdministrator;
					

			}
		}
		return null;
	}
	
	/**
	 * change password
	 * @param	administrator Administrator
	 * @return	Administrator
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public Administrator changePassword(Administrator administrator) {
		if( administrator != null && administrator.getUsername() != null ) {
			Administrator retrievedAdministrator = administratorDAO.findByUsername(administrator.getUsername());
			if ((retrievedAdministrator.getAccountState() == AccountState.Active)
					|| (retrievedAdministrator.getAccountState() == AccountState.PasswordReset)
					|| (retrievedAdministrator.getAccountState() == AccountState.UnVerfied)) {
				retrievedAdministrator.setAccountState(AccountState.Active);
				retrievedAdministrator.setNumberOfFailedAttempts(0);
				try {
					retrievedAdministrator.setPassword(sha256Password(administrator.getPassword()));
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Administrator newAdministrator =  administratorRepository.save(retrievedAdministrator);
				SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
				simpleMailMessage.setTo(newAdministrator.getEmail());
				simpleMailMessage.setSubject("Viddmonk Change Password Notification");
				simpleMailMessage.setText("Your password has been changed on Viddmonk site. If you have not changed the password please contact Viddmonk immediately.");
				sendMail.sendMail(simpleMailMessage);
				return newAdministrator;
			}
		}
		return null;
	}
	
	/**
	 * create SHA-256 password
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private String sha256Password(String password) throws NoSuchAlgorithmException {	    
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());
 
        byte byteData[] = md.digest();
 
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
 
        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<byteData.length;i++) {
    		String hex=Integer.toHexString(0xff & byteData[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	System.out.println("Hex format : " + hexString.toString());
    	return hexString.toString();
	}

}