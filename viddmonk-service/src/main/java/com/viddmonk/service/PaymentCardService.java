/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import java.util.List;

import com.viddmonk.domain.PaymentCard;

/**
 * Payment Service
 * @author rvadlam
 *
 */
public interface PaymentCardService {
	
	public PaymentCard findById(String id);
	
	public List<PaymentCard> findAll();
		
	public PaymentCard findByNickName(String nickname);
	
	public PaymentCard save(PaymentCard paymentCard);

}