/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.Media;

/**
 * Campaign Service
 * @author rvadlam
 *
 */
public interface CampaignService {
	
	public Campaign findById(String campaignId);
	
	public Media findMediaById(String mediaId);
	
	public Media saveMedia(Media media);
		
	public Campaign findByTitle(String title);
	
	public Campaign save(Campaign campaign);
	
	public Campaign save(Campaign campaign, MultipartFile videoFile);
	
	public Page<Campaign> getCampaignsByBrand(Brand brand, Pageable pageable);
	
	public Page<Campaign> findAll(Pageable pageable);

}