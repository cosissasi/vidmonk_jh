/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;


/**
 * Payment Service
 * @author rvadlam
 *
 */
public interface PaymentService {
	
	public String getAccount();

}