/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;


/**
 * Social Service
 * @author rvadlam
 *
 */
@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {
	
	
	public String getAccount() {
		BraintreeGateway gateway = new BraintreeGateway(
				  Environment.SANDBOX,
				  "dbvg67zbrzhdcfvq",
				  "3fs6pfyg9j72f4tf",
				  "df14ed9b373be95b895333e7c000473f"
				);
		TransactionRequest request = new TransactionRequest()
        .amount(new BigDecimal("1.00"))
        .creditCard()
          .number("4111111111111111")
          .expirationMonth("05")
          .expirationYear("2014")
          .done();

		Result<Transaction> result = gateway.transaction().sale(request);

	      if (result.isSuccess()) {
	          Transaction transaction = result.getTarget();
	          System.out.println("Success!: " + transaction.getId());
	      } else if (result.getTransaction() != null) {
	          System.out.println("Message: " + result.getMessage());
	          Transaction transaction = result.getTransaction();
	          System.out.println("Error processing transaction:");
	          System.out.println("  Status: " + transaction.getStatus());
	          System.out.println("  Code: " + transaction.getProcessorResponseCode());
	          System.out.println("  Text: " + transaction.getProcessorResponseText());
	      } else {
	          System.out.println("Message: " + result.getMessage());
	          for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
	              System.out.println("Attribute: " + error.getAttribute());
	              System.out.println("  Code: " + error.getCode());
	              System.out.println("  Message: " + error.getMessage());
	          }
	      }		
		return null;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		PaymentServiceImpl impl = new PaymentServiceImpl();
		
		impl.getAccount();
	}

}