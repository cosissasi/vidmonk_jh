/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.viddmonk.domain.Brand;

/**
 * Brand Service
 * @author rvadlam
 *
 */
public interface BrandService {
		
	
	public Brand findById(String id);

	public Brand findByName(String name);
	
	public Brand save(Brand brand);
	
	public Page<Brand> getBrandsByParentBrand(Brand brand, Pageable pageable);

}