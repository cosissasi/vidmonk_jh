/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.viddmonk.dao.AdministratorDAO;
import com.viddmonk.domain.Administrator;

/**
 * Administrator Detail Service
 * @author rvadlam
 *
 */
public class AdministratorDetailService implements UserDetailsService { 
	
	@Autowired 
	private AdministratorDAO administratorDAO; 
	
	/**
	 * load by username
	 * @param	username
	 * @return	UserDetails
	 * 
	 */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException 
	{ 
		Administrator administrator = administratorDAO.findByUsername(username);
		if (administrator == null) { 
			return null; 
		} 
		org.springframework.security.core.userdetails.User userDetail 
			= new org.springframework.security.core.userdetails.User(administrator.getUsername(),administrator.getPassword(),true,true,true,true,getAuthorities());
		return userDetail;
	} 

	/**
	 * get authorities
	 * @param role	
	 * @return
	 */
	public List<GrantedAuthority> getAuthorities() {
		 List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		 authList.add(new SimpleGrantedAuthority("ADMIN"));
		 return authList;
	}
}