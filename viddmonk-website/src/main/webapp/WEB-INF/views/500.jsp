<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>viddmonk - error page</title>
	    <%@include file="head.jsp" %>
	</head>

<body>
    <jsp:include page="topnav.jsp">
    	<jsp:param name="tabitem" value="dashboard" />
    </jsp:include>
    <div class="main-container" id="main-container">
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->

					<div class="error-container">
						<div class="well">
							<h1 class="grey lighter smaller">
								<span class="blue bigger-125">
									<i class="icon-random"></i>
									500
								</span>
								Something Went Wrong
							</h1>

							<hr />
							<h3 class="lighter smaller">
								But we are working
								<i class="icon-wrench icon-animated-wrench bigger-125"></i>
								on it!
							</h3>

							<div class="space"></div>

							<div>
								<h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

								<ul class="list-unstyled spaced inline bigger-110 margin-15">
									<li>
										<i class="icon-hand-right blue"></i>
										Read the faq
									</li>

									<li>
										<i class="icon-hand-right blue"></i>
										Give us more info on how this specific error occurred!
									</li>
								</ul>
							</div>

							<hr />
							<div class="space"></div>

							<div class="center">
								<a href="javascript: history.go(-1)" class="btn btn-grey">
									<i class="icon-arrow-left"></i>
									Go Back
								</a>
	
								<a href="<c:url value="/dashboard?page.page=${publicCampaignListPageNumber}&amp;page.size=4&amp;page.sort=createDate&amp;page.sort.dir=desc" />" class="btn btn-primary">
									<i class="icon-dashboard"></i>
									Dashboard
								</a>
							</div>
						</div>
					</div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div><!-- /.main-content -->
</body>
</html>