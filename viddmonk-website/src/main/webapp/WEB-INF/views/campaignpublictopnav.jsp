<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <!-- SECTION-1: TOPBAR NAV -->
    <div class="navbar navbar-default vdTitleBox" id="navbar">
	    <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
        </script>

        <div class="navbar-container no-padding-right" id="navbar-container">
            <div class="navbar-header pull-left">
                <a href="<c:url value="/dashboard"/>" class="navbar-brand no-padding">
                    <small>
                        <!-- <i class="icon-leaf"></i>  -->
                        <img src="<c:url value="/resources/assets/images/viddMonk-30.png"/>" alt="viddMonk">
                        viddmonk
                    </small>
                </a><!-- /.brand -->
            </div><!-- /.navbar-header -->
        </div><!-- /.container -->
    </div>
    <!-- end of top-nav bar -->
