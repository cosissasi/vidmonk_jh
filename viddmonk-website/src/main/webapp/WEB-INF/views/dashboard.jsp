<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>viddMonk - Dashboard</title>
	    <%@include file="head.jsp" %>
	</head>

<body>
    <jsp:include page="topnav.jsp">
    	<jsp:param name="tabitem" value="dashboard" />
    </jsp:include>

    <!-- SECTION-2: MAIN CONTAINER AND SIDEBAR NAV -->
    <div class="main-container" id="main-container">
        <div class="page-content"><!-- PAGE CONTENT BEGINS -->
        	<div class="page-content-inner">
                <div class="row">
                    <h3 class="page-header">Your Campaigns
                        <small>
                            <i class="icon-double-angle-right"></i>
                            Campaigns created by you
                        </small>
                    </h3>
                    <div>
                    	<ul class="pagination">
					    	<c:if test="${myCampaignListFirstPage == false}">
								<li><a href="<c:url value="/dashboard?page.page=${publicCampaignListPageNumber}&amp;page.size=4&amp;page.sort=createDate&amp;page.sort.dir=desc" />">Prev</a></li>
					    	</c:if>
					    	<c:if test="${myCampaignListLastPage == false}">
								<li><a href="<c:url value="/dashboard?page.page=${publicCampaignListPageNumber+2}&amp;page.size=4&amp;page.sort=createDate&amp;page.sort.dir=desc" />">Next</a></li>
					    	</c:if>
		  				</ul>
                    </div>
                    
		     		<div class="text-center"> <!-- CAMPAIGN CARDS -->                	
						
  					
						<c:forEach items="${myCampaignList}" var="campaign">
							<c:set var="desc" value="${campaign.description}"/>
			                <div class="infocontainersmall align-center">
	                        	<div class="infocontainerimagecontainer">
			                        <a href="<c:url value="/secure/campaigns/dashboard/${campaign.id}"/>">
										<c:forEach items="${campaign.campaignImageList}" var="media" begin="0" end="0">                        
					                    	<img src="<c:url value="${media.s3URL}"/>"  class="infocontainerimage">
				                        </c:forEach>      
			                        </a>
			                    </div>
	                          	<a  style="text-decoration: none" href="<c:url value="/secure/campaigns/dashboard/${campaign.id}"/>">
			                        <h4 class="vdTitle smaller ">${campaign.title}</h4>                                
			                        <div class="fluid-row" >
		                                <div class="col-xs-3">
		                                    <p class="icon-time no-margin-bottom"></p>
		                                    <p>${campaign.numberOfDaysActive} days</p>
		                                </div>
		                                <div class="col-xs-3 ">
		                                    <p class="icon-eye-open no-margin-bottom"></p>
		                                    <p> ${campaign.numberOfViews} </p>
		                                </div>
		                                <div class="col-xs-3 ">
		                                    <p class="icon-film no-margin-bottom"></p>
		                                    <p>${fn:length(campaign.creativeVideoList)}</p>
		                                </div>
		                                <div class="col-xs-3 ">
		                                    <p class="icon-usd no-margin-bottom"></p>
		                                    <p>Impl.</p>
		                                </div>
	                            	</div>		                        	                                	
	                          	</a>
		                 	</div>
						</c:forEach>                            

			            <div class="infocontainersmall infocontainersmallnew center">
			              <div class="infocontainerimagecontainer">
			                  <a href="<c:url value="/secure/campaigns/add"/>"><img src="<c:url value="/resources/assets/images/gallery/monk.jpg"/>" class="infocontainerimage"></a>
			              </div>
			              <a href="<c:url value="/secure/campaigns/add"/>" class="btn btn-success btn-block">Start New Campaign</a>
			              <h4 class="white smaller ">Create New Campaign</h4>
			            </div>
	            	</div>
            	</div>
            </div>                        
        </div><!-- PAGE CONTENT ENDS-->
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
           	<i class="icon-double-angle-up icon-only bigger-110"></i>
       	</a>        
    </div><!-- /.main-content -->

    <!-- ======== BASIC SCRIPTS ==========-->
    <!-- basic scripts -->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-1.10.2.min.js"/>'>"+"<"+"/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
    </script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/typeahead-bs2.min.js"/>"></script>

    <!-- page specific plugin scripts -->

    <!-- ace scripts -->

    <script src="<c:url value="/resources/assets/js/ace-elements.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/ace.min.js"/>"></script>
    
    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($) {            
            $('.infocontainersmall').on({
                mouseenter: function() {
                    $(this).addClass("infocontainersmallhover");
                },
                mouseleave: function() {
                    $("div").removeClass("infocontainersmallhover");
                }
            });

        });
    </script>
</body>
</html>