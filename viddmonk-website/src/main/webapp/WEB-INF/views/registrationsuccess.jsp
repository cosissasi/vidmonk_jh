<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>
	<meta charset="utf-8" />
	<title>viddMonk - Login</title>
	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
	<link href="<c:url value="/resources/assets/css/bootstrap.min.css"/>" rel="stylesheet" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome.min.css"/>" />
	<!--[if IE 7]>
	  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
	<![endif]-->
	<!-- page specific plugin styles -->
	<!-- fonts -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-fonts.css"/>" />
	<!-- ace styles -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-rtl.min.css"/>" />
	<!--[if lte IE 8]>
	  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
	<![endif]-->
	<!-- inline styles related to this page -->
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
                                    <img src="<c:url value="/resources/assets/images/viddMonk-30.png"/>" alt="viddMonk">
									<span class="text-info green">Welcome to viddMonk</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger center">
												<i class="icon-gears green"></i>
												 Registration Successful
											</h4>

											<div class="space-6"></div>

											<p> Your registration was successful. Login Instructions were sent to your email. </p>
										</div><!-- /widget-main -->

										<div class="toolbar center">
											<a href="<c:url value="/auth/login"/>" class="back-to-login-link">
												Back to login
												<i class="icon-arrow-right"></i>
											</a>
										</div>

									</div><!-- /widget-body -->
								</div><!-- /login-box -->							
							</div><!-- /position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-1.10.2.min.js"/>'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			function show_box(id) {
			 jQuery('.widget-box.visible').removeClass('visible');
			 jQuery('#'+id).addClass('visible');
			}
		</script>
	</body>
</html>