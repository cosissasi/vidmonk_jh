<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>viddMonk create campaign - Videos and Compensation</title>
	    <%@include file="head-createcampaign.jsp" %>
	    <script type="text/javascript">
		    $(function(){
		    	$('#poster-original').Jcrop({
		    		onChange: showCoords,
		    		onSelect: showCoords
		    	});
		    });	    
		    function showCoords(c)
		    {
		    	$('#x1').val(c.x);
		    	$('#y1').val(c.y);
		    	$('#x2').val(c.x2);
		    	$('#y2').val(c.y2);
		    	$('#w').val(c.w);
		    	$('#h').val(c.h);
		    };
		</script>
	</head>

<body>

    <%@include file="topnav.jsp" %>


    <!-- SECTION-2: MAIN CONTAINER AND SIDEBAR NAV -->
    <div class="main-container" id="main-container">
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-blue widget-header-flat">
                                            <h4 class="lighter">New Campaign</h4>
                                        </div>
                                        <div class="widget-body ">
                                            <div class="widget-main">
                                                <div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
                                                    <ul class="wizard-steps">
                                                        <li data-target="#step1" class="complete">
                                                            <a href="<c:url value="/secure/campaigns/update/${campaign.id}"/>"><span class="step">1</span></a>
                                                            <span class="title">Setup</span>
                                                        </li>

                                                        <li data-target="#step2" class="active">
                                                            <span class="step">2</span>
                                                            <span class="title">Describe</span>
                                                        </li>

                                                        <li data-target="#step3">
                                                            <span class="step">3</span>
                                                            <span class="title">Reward</span>
                                                        </li>

                                                        <li data-target="#step4">
                                                            <span class="step">4</span>
                                                            <span class="title">Pay</span>
                                                        </li>
                                                    </ul>
                                                </div>

                                               <div class="step-pane" id="step2"> <!-- STEP-2 -->
                                                  <form name="createcampaignform" id="createcampaignform" class="form-horizontal" method="POST" action="<c:url value="/secure/campaigns/add"/>" accept-charset="UTF-8"  enctype="multipart/form-data">
                                                       <div class="row">
                                                           <h3 class="header smaller lighter green center"></h3>
	                                                        <input type="hidden" id="campaignId" name="campaignId" value="${campaign.id}" /> 
	                                                        <input type="hidden" id="brandId" name="brandId" value="${campaign.brand.id}" /> 
	                                                        <input type="hidden" id="step" name="step" value="3" /> 
                                                           <div class="form-group">
                                                               <label class="col-sm-3 control-label no-padding-right">Length</label>
                                                               <div class="col-sm-8 control-group">
                                                                   <div class="radio no-padding-left no-padding-top">
                                                                       <select class="form-control" id="videoLength" name="videoLength">
                                                                       	   <c:if test="${campaign.creativeVideoLength == '6' }">
	                                                                            <option value="6" selected="selected">6 Seconds (Vine like)</option>
                                                                           </c:if>
                                                                       	   <c:if test="${empty campaign.creativeVideoLength || campaign.creativeVideoLength != '6' }">
	                                                                            <option value="6">6 Seconds (Vine like)</option>
                                                                           </c:if>
                                                                       	   <c:if test="${campaign.creativeVideoLength == '15' }">
	                                                                            <option value="15" selected="selected">15 Seconds (Instagram like)</option>
                                                                           </c:if>
                                                                       	   <c:if test="${empty campaign.creativeVideoLength || campaign.creativeVideoLength != '15' }">
	                                                                            <option value="15">15 Seconds (Instagram like)</option>
                                                                           </c:if>
                                                                       	   <c:if test="${campaign.creativeVideoLength == '30' }">
	                                                                            <option value="30" selected="selected">30 Seconds </option>
                                                                           </c:if>
                                                                       	   <c:if test="${empty campaign.creativeVideoLength || campaign.creativeVideoLength != '30' }">
	                                                                            <option value="30">30 Seconds </option>
                                                                           </c:if>
                                                                       	   <c:if test="${campaign.creativeVideoLength == '60' }">
	                                                                            <option value="60" selected="selected">60 Seconds </option>
                                                                           </c:if>
                                                                       	   <c:if test="${empty campaign.creativeVideoLength || campaign.creativeVideoLength != '60' }">
	                                                                            <option value="60">60 Seconds </option>
                                                                           </c:if>
                                                                        </select>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="form-group">
                                                               <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Video tags</label>
                                                               <div class="col-sm-8">
                                                                   <input type="text" name="tags" id="form-field-tags" placeholder="Enter tags ..."  value="${campaign.tags}"/>
                                                               </div>
                                                           </div>
                                                           <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-3"> Instructions </label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="form-control limited" id="instructions" name="instructions" placeholder="Special Instructions for the Videos you want" maxlength="256"> ${campaign.instructions}</textarea>
                                                                </div>
                                                           </div>	
                                                           <div class="form-group">
                                                               <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Poster</label>
                                                               <div class="col-sm-8 ">
                                                                   <div class="widget-main no-padding-left">
                                                                       <input type="file" id="id-input-file-2" name="id-input-file-2" />
                                                                   </div>
		                                                            <div class="col-sm-4">
			 															<c:forEach items="${campaign.campaignImageList}" var="media" begin="0" end="0">                        
			                                                                <ul class="ace-thumbnails">
			                                                                    <li>
			                                                                    	<br /><br/><br/><img alt="poster" src="${media.s3URL}" id="poster-original" />
			                                                                        <div class="tools">
			                                                                            <a href="#">
			                                                                                <i class="icon-remove red"></i>
			                                                                            </a>
			                                                                        </div>
			                                                                    </li>
			                                                                </ul>
			                                                            </c:forEach>      
		                                                            </div>
	                                                               <div class="inline-labels">
	                                                                   <input type="hidden" size="4" id="x1" name="x1" />
	                                                                   <input type="hidden" size="4" id="y1" name="y1" />
	                                                                   <input type="hidden" size="4" id="x2" name="x2" />
	                                                                   <input type="hidden" size="4" id="y2" name="y2" />
	                                                                   <input type="hidden" size="4" id="w" name="w" />
	                                                                   <input type="hidden" size="4" id="h" name="h" />
	                                                               </div>
                                                               </div>
                                                           </div>
                                                       </div>
				                                       <div class="row-fluid wizard-actions">
				                                            <button class="btn btn-vddarkgrey" onclick="return save2();">
				                                                <i class="icon-save"></i>
				                                                Save
				                                            </button>
				                                            <button class="btn btn-prev btn-vdlightgrey"  onclick="return save();">
				                                                <i class="icon-arrow-left"></i>
				                                                Prev
				                                            </button>
				                                            <button class="btn btn-next btn-vdsalmon" data-last="Finish " onclick="return next2();">
				                                                Next
				                                                <i class="icon-arrow-right icon-on-right"></i>
				                                            </button>
				                                       </div>
                                                   </form>
                                               </div>
                                            </div><!-- widget main -->
                                        </div><!-- widget body -->
                                    </div><!-- widget box -->
                                </div>
                            </div>
                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->                
    </div><!-- /.main-container-inner -->

    <%@include file="createcampaignfooter.jsp" %>
        
</body>
</html>