<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>viddMonk create campaign - Videos and Compensation</title>
	    <%@include file="head-createcampaign.jsp" %>
	</head>

<body>

    <%@include file="topnav.jsp" %>


    <!-- SECTION-2: MAIN CONTAINER AND SIDEBAR NAV -->
    <div class="main-container" id="main-container">
        <div class="main-content">               
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-blue widget-header-flat">
                                            <h4 class="lighter"></h4>
                                        </div>
                                        <div class="widget-body ">
                                            <div class="widget-main">
                                                <div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
                                                    <ul class="wizard-steps">
                                                        <li data-target="#step1" class="complete">
                                                            <a href="<c:url value="/secure/campaigns/update/${campaign.id}"/>"><span class="step">1</span></a>
                                                            <span class="title">Setup</span>
                                                        </li>

                                                        <li data-target="#step2" class="complete">
                                                            <a href="<c:url value="/secure/campaigns/update/${campaign.id}?step=2"/>"><span class="step">2</span></a>
                                                            <span class="title">Describe</span>
                                                        </li>

                                                        <li data-target="#step3" class="complete">
                                                            <a href="<c:url value="/secure/campaigns/update/${campaign.id}?step=3"/>"><span class="step">3</span></a>
                                                            <span class="title">Reward</span>
                                                        </li>

                                                        <li data-target="#step4">
                                                            <span class="step">4</span>
                                                            <span class="title">Pay</span>
                                                        </li>
                                                    </ul>
													<div class="header smaller lighter center green"></div>
                                                </div>
                                            <div class="step-pane" id="step4"> <!-- STEP-4 FILES -->
	                                           <form name="createcampaignform" id="createcampaignform" class="form-horizontal" method="POST" action="<c:url value="/secure/campaigns/add"/>" accept-charset="UTF-8"  enctype="multipart/form-data">
    	                                            <input type="hidden" id="campaignId" name="campaignId" value="${campaign.id}" /> 
	                                                <input type="hidden" id="brandId" name="brandId" value="${campaign.brand.id}" /> 
	                                                <input type="hidden" id="step" name="step" value="4" /> 
                                                    <div class="space-6"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-instr"> Payment Type </label>
                                                        <div class="col-sm-8">
                                                            <div class="radio no-padding-left no-padding-top">
                                                                <label>
                                                                    <input name="form-field-radio" type="radio" class="ace trigger" data-rel="credit-card" />
                                                                    <span class="lbl"> Credit-Card </span>
                                                                </label>
                                                            </div>
                                                            <div class="radio no-padding-left no-padding-top">
                                                                <label>
                                                                    <input name="form-field-radio" type="radio" class="ace trigger" data-rel="paypal" />
                                                                    <span class="lbl"> PayPal </span>
                                                                </label>
                                                            </div>
                                                            <hr/>
                                                        </div>
                                                        <div class="credit-card content">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" > Credit Card </label>
                                                                <div class="col-sm-8">
                                                                     <span class="input-icon input-icon-right">
                                                                        <input type="text" id="credit-card-number" placeholder="Card Number" />
                                                                        <i class="icon-credit-card green"></i>
                                                                     </span>
                                                                </div>
                                                                <div class="space-4"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> Name </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-name" placeholder="Name on Card" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> Expiration Date </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-month" placeholder="MM" class="col-xs-4 col-sm-1"/>
                                                                    <input type="text" id="credit-card-year" placeholder="YY" class="col-xs-4 col-sm-1"/>
                                                                    <label class="col-sm-3 col-xs-10 control-label no-padding-right"> Security Code </label>
                                                                    <div class="col-sm-2">
                                                                        <input type="text" id="credit-card-security-code" class="col-xs-6"/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="row">
                                                                <label class="control-label green col-sm-3">Billing details</label>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> Address </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-address" placeholder="Address" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> Address 2 </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="fcredit-card-address2" placeholder="optional" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> City </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-city" placeholder="City" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> State/Province </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-city" placeholder="State/Province" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"> Zip Code </label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="credit-card-zip" placeholder="Zip code" class="col-xs-10 col-sm-6"/>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                        </div>

                                                        <div class="form-group paypal content">
                                                            <label class="col-sm-3 control-label no-padding-right"> Details </label>
                                                            <div class=" col-sm-8 ">
                                                                <div class="alert alert-info">
                                                                    You will be redirected to PayPal to enter your credentials
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
				                                        <div class="row-fluid wizard-actions">
				                                            <button class="btn btn-vddarkgrey growl-type" data-type="success" onclick="return save4();">
				                                                <i class="icon-save"></i>
				                                                Save
				                                            </button>
				                                            <button class="btn btn-prev btn-vdlightgrey" onclick="return save3();">
				                                                <i class="icon-arrow-left"></i>
				                                                Prev
				                                            </button>
				                                            <button class="btn btn-next btn-vdsalmon growl-type" data-type="success" data-last="Finish " onclick="return finish();">
				                                                Finish
				                                                <i class="icon-arrow-right icon-on-right"></i>
				                                            </button>
				                                        </div>                                           
                                                </form>
                                            </div> <!-- end of step-4 -->                                               
                                        </div><!-- widget body -->
                                    </div><!-- widget box -->
                                </div>
                            </div>
                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->                
    </div><!-- /.main-container-inner -->

    <%@include file="createcampaignfooter.jsp" %>
        
</body>
</html>