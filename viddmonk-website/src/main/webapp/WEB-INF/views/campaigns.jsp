<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Viddmonk Brands Page</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />    
    
    <link href='<c:url value="/resources/css/bootstrap.min.css"/>' rel="stylesheet">
    <link href='<c:url value="/resources/css/bootstrap-responsive.min.css"/>' rel="stylesheet">    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href='<c:url value="/resources/css/font-awesome.min.css"/>' rel="stylesheet">        
    <link href='<c:url value="/resources/css/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>' rel="stylesheet">            
    <link href='<c:url value="/resources/css/base-admin-2.css"/>' rel="stylesheet">            
    <link href='<c:url value="/resources/css/base-admin-2-responsive.css"/>' rel="stylesheet">                
    <link href='<c:url value="/resources/css/pages/dashboard.css"/>' rel="stylesheet">                
    <link href='<c:url value="/resources/css/custom.css"/>' rel="stylesheet">                
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
    <%@include file="topnav.jsp" %>
    
    <div class="main">
	    <div class="container">
	      <div class="row">  	
	      	<div class="span12">      		  		
	      		<div class="widget stacked ">  			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Manage Your Campaigns</h3>
	  				</div> <!-- /widget-header -->				
					<div class="widget-content">				
						<br />			
						<section id="buttons">
							<a href="<c:url value="/secure/campaigns/add" />" class="btn btn-large btn-primary">Add New Campaign</a>&nbsp;&nbsp;&nbsp;
						</section>
						<br />
						<br />
						<section id="tables">
							<h3>Your Campaigns </h3>
							<table class="table table-bordered table-striped table-highlight">
						        <thead>
						          <tr>
						            <th>Title</th>
						            <th>Tag Line</th>
						            <th>Short Description</th>
						            <th>Description</th>
						            <th>Instructions</th>
						            <th>Start Date</th>
						            <th>End Date</th>
						            <th class="td-actions"></th>
						          </tr>
						        </thead>
						        <tbody>
								    <c:if test="${empty myCampaignList}">
										<tr>
											<td colspan="6">No Campaigns Found</td>
										</tr>
								    </c:if>
									<c:forEach items="${myCampaignList}" var="campaign">
										<tr>
											<td>${campaign.title}</td>
											<td>${campaign.tagline}</td>
											<td>${campaign.shortDescrption}</td>
											<td>${campaign.description}</td>
											<td>${campaign.instructions}</td>
											<td></td>
											<td></td>
											<td class="td-actions">
												<a href="javascript:;" class="btn btn-small">
													<i class="btn-icon-only icon-remove"></i>										
												</a>
											</td>
										</tr>
									</c:forEach>
						    	</tbody>
						    </table>
						</section>						
					</div>
				</div>
			</div>
		  </div>
		</div>
	</div>    
    
    <%@include file="footer.jsp" %>
</body>
</html>