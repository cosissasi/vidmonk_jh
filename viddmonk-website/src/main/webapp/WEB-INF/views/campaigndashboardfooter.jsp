   <!-- ======== BASIC SCRIPTS ==========-->
    <!-- basic scripts -->
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/assets/js/jquery-2.0.3.min.js" /> "></script>

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
    </script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/typeahead-bs2.min.js"/>"></script>

    <!-- page specific plugin scripts -->
    <script src="<c:url value="/resources/assets/js/jquery.knob.min.js"/>"></script>

    <!-- ace scripts -->

    <script src="<c:url value="/resources/assets/js/ace-elements.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/ace.min.js"/>"></script>

	<!-- inline scripts related to this page -->
	<script type="text/javascript">
	    jQuery(function($) {
	        $(".knob").knob();
	        $('#loading-btn').on(ace.click_event, function () {
	            var btn = $(this);
	            btn.button('loading')
	            setTimeout(function () {
	                btn.button('reset')
	            }, 2000)
	        });
	
	        $('#id-button-borders').attr('checked' , 'checked').on('click', function(){
	            $('#default-buttons .btn').toggleClass('no-border');
	        });
	    });
	</script>

	<script type="text/javascript">	
		function statuschange(campaignid, mediaId, status) {
			$.getJSON("<c:url value="/secure/campaigns/media/statuschange"/>", { campaignId: campaignid, mediaid: mediaId, campaignstatus: status.checked }, function(statusupdated) {
			});
		}
	</script>    
    
    