package com.viddmonk.website;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.CampaignAdministrator;
import com.viddmonk.domain.CampaignStatus;
import com.viddmonk.domain.Media;
import com.viddmonk.domain.MediaStatus;
import com.viddmonk.domain.PaymentCard;
import com.viddmonk.service.AdministratorService;
import com.viddmonk.service.CampaignService;
import com.viddmonk.service.PaymentCardService;

@Controller
@SessionAttributes("email")
public class CampaignController {
	
	private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private PaymentCardService paymentCardService;
	

	/**
	 * get all campaigns per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns"}, method = RequestMethod.GET)
	public String getAllCampaigns(Locale locale, Model model, Principal principal) {
		logger.info("Dashboard landing page");
		Page<CampaignAdministrator> myCampaignAdministratorList = administratorService.myCampaigns(principal.getName(), new PageRequest(0, 5));
		List<Campaign> myCampaignList = new ArrayList<Campaign>();
		if( myCampaignAdministratorList != null ) {
        	for(CampaignAdministrator campaignAdministrator : myCampaignAdministratorList.getContent()) {
        		myCampaignList.add(campaignAdministrator.getCampaign());
        	}
        }
		model.addAttribute("myCampaignList", myCampaignList);
		return "campaigns";
	}
	
	/**
	 * campaign dashboard 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/dashboard/{campaignId}"}, method = RequestMethod.GET)
	public String campaigndashobard(Locale locale, Model model, Principal principal, @PathVariable String campaignId) {
		logger.info("Update A Campaign display page");
		if( campaignId != null ) {
			Campaign campaign = campaignService.findById(campaignId);
			model.addAttribute("campaign", campaign);
		}
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "campaigndashboard";		
	}
	
	/**
	 * campaign public page 
	 * 
	 */	
	@RequestMapping(value = {"/public/campaigns/public/{campaignId}"}, method = RequestMethod.GET)
	public String campaignpublic(Locale locale, Model model, @PathVariable String campaignId) {
		logger.info("Campaign Public display page");
		if( campaignId != null ) {			
			Campaign campaign = campaignService.findById(campaignId);
			if( campaign != null ) {
				campaign.setNumberOfViews(campaign.getNumberOfViews() + 1);
				campaign = campaignService.save(campaign);
			}
			model.addAttribute("campaign", campaign);
		}
		return "campaignpublic";		
	}
	
	/**
	 * upload video to campaign 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/campaign/uploadvideo"}, method = RequestMethod.POST)
	public String addEventSubmit(
			Locale locale,
			Model model,
			Principal principal,
			@RequestParam(value = "campaignId", required = true) String campaignId,
			@RequestParam(value = "videoFile", required = false) MultipartFile videoFile) {		
		logger.info("Upload Video");
		if( campaignId != null ) {
			Campaign campaign = campaignService.findById(campaignId);
			campaignService.save(campaign, videoFile);
			model.addAttribute("campaign", campaign);
		}
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "campaignpublic";		
	}
	
	/**
	 * add a campaign 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/add"}, method = RequestMethod.GET)
	public String addCampaign(Locale locale, Model model, Principal principal) {
		logger.info("Add A Campaign display page");
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "createcampaign";
	}
	
	/**
	 * update a campaign 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/update/{campaignId}"}, method = RequestMethod.GET)
	public String updateCampaign(Locale locale, Model model, Principal principal, @PathVariable String campaignId) {
		logger.info("Update A Campaign display page");
		if( campaignId != null ) {
			Campaign campaign = campaignService.findById(campaignId);
			model.addAttribute("campaign", campaign);			
		}
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "createcampaign";
	}
	
	/**
	 * update status 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/media/statuschange"}, method = {RequestMethod.POST, RequestMethod.GET})
	public  @ResponseBody String changeStatus(Locale locale, Model model, Principal principal,
		@RequestParam(value = "campaignId", required = true) String campaignId,
		@RequestParam(value = "mediaid", required = true) String mediaid,
		@RequestParam(value = "campaignstatus", required = true) String campaignstatus ) {
		logger.info("Update A Campaign display page");
		if( mediaid != null && campaignId != null ) {
			Media media = campaignService.findMediaById(mediaid);
			if( campaignstatus != null && "false".equals(campaignstatus) ) {
				media.setMediaStatus(MediaStatus.REJECTED);
				campaignService.saveMedia(media);
			} else if( campaignstatus != null && "true".equals(campaignstatus) ) {
				media.setMediaStatus(MediaStatus.APPROVED);
				campaignService.saveMedia(media);				
			}
		}
		return "statusupdated";
	}
	
	
	/**
	 * get all campaigns per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/campaigns/save"}, method = RequestMethod.POST)
	public String addCampaignSubmit(
			Locale locale,
			Model model,
			Principal principal,
			@RequestParam(value = "step", required = false) String step,
			@RequestParam(value = "brandId", required = false) String brandId,
			@RequestParam(value = "brandName", required = false) String brandName,
			@RequestParam(value = "campaignId", required = false) String campaignId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "tagline", required = false) String tagline,
			@RequestParam(value = "date-range-picker", required = false) String dateRange,
			@RequestParam(value = "id-input-file-2", required = false) List<MultipartFile> files,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "instructions", required = false) String instructions,
			@RequestParam(value = "nickname", required = false) String nickname,
			@RequestParam(value = "nicknameSelection", required = false) String nicknameSelection,
			@RequestParam(value = "creditCardNumber", required = false) String creditCardNumber,
			@RequestParam(value = "cardholderName", required = false) String cardholderName,
			@RequestParam(value = "securityCode", required = false) String securityCode,
			@RequestParam(value = "address", required = false) String address, 
			@RequestParam(value = "tags", required = false) String tags,
			@RequestParam(value = "videoLength", required = false) String videoLength,
			@RequestParam(value = "competitive", required = false) Boolean competitive,
			@RequestParam(value = "competitive1st", required = false) String competitive1st,
			@RequestParam(value = "competitive2nd", required = false) String competitive2nd,
			@RequestParam(value = "competitive3rd", required = false) String competitive3rd,
			@RequestParam(value = "performance", required = false) Boolean performance,
			@RequestParam(value = "awardByViews", required = false) String awardByViews,
			@RequestParam(value = "awardByLikes", required = false) String awardByLikes,
			@RequestParam(value = "x1", required = false) String x1,
			@RequestParam(value = "y1", required = false) String y1,
			@RequestParam(value = "x2", required = false) String x2,
			@RequestParam(value = "y2", required = false) String y2,
			@RequestParam(value = "w", required = false) String w,
			@RequestParam(value = "h", required = false) String h,
			@RequestParam(value = "fixedPrize", required = false) Boolean fixedPrize,
			@RequestParam(value = "fixedPrizeAmount", required = false) String fixedPrizeAmount,
			@RequestParam(value = "fixedPrizeQuantity", required = false) String fixedPrizeQuantity,
			@RequestParam(value = "smallReward", required = false) Boolean smallReward,
			@RequestParam(value = "smallreward-date-range-picker", required = false) String smallRewardDateRangePicker,
			@RequestParam(value = "smallRewardDescription", required = false) String smallRewardDescription,
			@RequestParam(value = "guaranteeRewards", required = false) Boolean guaranteeRewards
			) {
		
		logger.info("Add/Update A Campaign page");
		Campaign campaign;
		if( campaignId != null && !"".equals(campaignId) ) {
			campaign = campaignService.findById(campaignId);
			if( campaign == null ) {
				campaign = new Campaign();				
			}
			
		} else {
			campaign = new Campaign();
			campaign.setCampaignStatus(CampaignStatus.CREATED);
		}
		if( title != null && !"".equals(title) ) {
			campaign.setTitle(title.trim());			
		}
		if( tagline != null && !"".equals(tagline) ) {
			campaign.setTagline(tagline.trim());
		}
		if( description != null && !"".equals(description) ) {
			campaign.setDescription(description.trim());
		}
		if( dateRange != null ) {
			String[] dates = dateRange.split("-");
			if( dates != null && dates.length == 2 ) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date startdate = null;
				Date enddate = null;
				try {
					startdate = sdf.parse(dates[0]);
					enddate = sdf.parse(dates[1]);
					campaign.setStartDateTime(startdate);
					campaign.setEndDateTime(enddate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if( videoLength != null ) {
			campaign.setCreativeVideoLength(videoLength);
		}
		if( videoLength != null ) {
			campaign.setCreativeVideoLength(videoLength);
		}
		if( tags != null ) {
			campaign.setTags(tags);
		}
		if( instructions != null && !"".equals(instructions) ) {
			campaign.setInstructions(instructions.trim());
		}
		if( x1 != null && !"".equals(x1) ) {
			campaign.setX1(x1);
		}
		if( x2 != null && !"".equals(x2) ) {
			campaign.setX2(x2);
		}
		if( y1 != null && !"".equals(y1) ) {
			campaign.setY1(y1);
		}
		if( y2 != null && !"".equals(y2) ) {
			campaign.setY2(y2);
		}
		if( w != null && !"".equals(w) ) {
			campaign.setW(w);
		}
		if( h != null && !"".equals(h) ) {
			campaign.setH(h);
		}
		if (x1 != null && !"".equals(x1) && x2 != null && !"".equals(x2)
				&& y1 != null && !"".equals(y1) && y2 != null && !"".equals(y2)
				&& w != null && !"".equals(w) && h != null && !"".equals(h)) {
			administratorService.saveCroppedImage(campaign);
		}
		List<MultipartFile> imageFiles = new ArrayList<MultipartFile>();
		List<MultipartFile> audioFiles = new ArrayList<MultipartFile>();
		List<MultipartFile> videoFiles = new ArrayList<MultipartFile>();
		if( files != null ) {
			for(MultipartFile multipartFile : files) {
				if (multipartFile.getContentType() != null
						&& (multipartFile.getContentType().contains("png")
								|| multipartFile.getContentType().contains("jpg")
								|| multipartFile.getContentType().contains("jpeg")
								|| multipartFile.getContentType().contains("gif") || multipartFile
								.getName().contains("bmp"))) {
					imageFiles.add(multipartFile);
				}
				if (multipartFile.getContentType() != null
						&& (multipartFile.getContentType().contains("mp3"))) {
					audioFiles.add(multipartFile);
				}
				if (multipartFile.getContentType() != null
						&& (multipartFile.getContentType().contains("mp4"))) {
					videoFiles.add(multipartFile);
				}
			}
		}
		if( nicknameSelection != null ) {
			PaymentCard paymentCard = paymentCardService.findByNickName(nicknameSelection);
			if( paymentCard == null ) {
				paymentCard = new PaymentCard();
			}
			paymentCard.setAddress(address);
			paymentCard.setCardholderName(cardholderName);
			paymentCard.setCreditCardNumber(creditCardNumber);
			paymentCard.setSecurityCode(securityCode);
			paymentCardService.save(paymentCard);
			campaign.getPaymentCardList().add(paymentCard);
		}
		if( brandId == null && brandName != null && !"".equals(brandName) ) {
			Brand brand = new Brand();
			brand.setName(brandName);
			brand = administratorService.saveBrand(brand, principal.getName(),null);
			if( brand != null ) {
				brandId = brand.getId();
			}
		}
		if( competitive != null ) {
			campaign.setCompetitive(competitive);			
		}
		if( competitive1st != null ) {
			try {
				campaign.setCompetitive1st(new Double(competitive1st));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}			
		}
		if( competitive2nd != null ) {
			try {
				campaign.setCompetitive2nd(new Double(competitive2nd));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}			
		}
		if( competitive3rd != null ) {
			try {
				campaign.setCompetitive3rd(new Double(competitive3rd));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		if( performance != null ) {
			campaign.setPerformance(performance);			
		}
		if( awardByViews != null ) {
			try {
				campaign.setAwardByViews(new Double(awardByViews));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		if( awardByViews != null ) {
			try {
				campaign.setAwardByLikes(new Double(awardByLikes));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		if( fixedPrize != null ) {
			campaign.setFixedPrize(fixedPrize);			
		}
		if( fixedPrizeAmount != null ) {
			try {
				campaign.setFixedPrizeAmount(new Double(fixedPrizeAmount));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		if( fixedPrizeQuantity != null ) {
			try {
				campaign.setFixedPrizeQuantity(new Integer(fixedPrizeQuantity));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		if( smallReward != null ) {
			campaign.setSmallReward(smallReward);
		}
		if( smallRewardDescription != null && !"".equals(smallRewardDescription) ) {
			campaign.setSmallRewardDescription(smallRewardDescription.trim());			
		}
		if( guaranteeRewards != null && !"".equals(guaranteeRewards) ) {
			campaign.setGuaranteeRewards(guaranteeRewards);			
		}
		if( smallRewardDateRangePicker != null ) {
			String[] dates = smallRewardDateRangePicker.split("-");
			if( dates != null && dates.length == 2 ) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date startdate = null;
				Date enddate = null;
				try {
					startdate = sdf.parse(dates[0]);
					enddate = sdf.parse(dates[1]);
					campaign.setSmallRewardExpiryStartDateTime(startdate);
					campaign.setSmallRewardExpiryEndDateTime(enddate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		Campaign newcampaign = administratorService.saveCampaign(campaign, brandId, principal.getName(), imageFiles, audioFiles, videoFiles);
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		model.addAttribute("campaign", newcampaign);
		List<PaymentCard> myPaymentCardList = paymentCardService.findAll();
		model.addAttribute("myPaymentCardList", myPaymentCardList);
		if( step != null && "1".equals(step) ) {
			return "createcampaign";			
		} else if ( step != null && "2".equals(step) ) {
			return "createcampaign-step2";				
		} else if ( step != null && "3".equals(step) ) {
			return "createcampaign-step3";				
		} else if ( step != null && "4".equals(step) ) {
			return "createcampaign-step4";				
		} else if ( step != null && "finish".equals(step) ) {
			Administrator administrator = administratorService.findByUsername(principal.getName());	
			Page<CampaignAdministrator> myCampaignAdministratorList = administratorService.myCampaigns(principal.getName(), new PageRequest(0, 5));
			List<Campaign> myCampaignList = new ArrayList<Campaign>();
			if( myCampaignAdministratorList != null ) {
	        	for(CampaignAdministrator campaignAdministrator : myCampaignAdministratorList.getContent()) {
	        		myCampaignList.add(campaignAdministrator.getCampaign());
	        	}
	        }
			model.addAttribute("myCampaignList", myCampaignList);
			model.addAttribute("numberOfBrands", myBrandList.size());
			model.addAttribute("numberOfCampaigns", myCampaignList.size());
			model.addAttribute("numberOfUsers", 0);
			model.addAttribute("myBrandList", myBrandList);
			model.addAttribute("myCampaignList", myCampaignList);
			model.addAttribute("username",administrator.getName());
			return "dashboard";				
		}
		return "createcampaign";
	}
}