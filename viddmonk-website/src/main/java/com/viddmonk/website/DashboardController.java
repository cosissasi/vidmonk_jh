package com.viddmonk.website;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.CampaignAdministrator;
import com.viddmonk.service.AdministratorService;
import com.viddmonk.service.CampaignService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("email")
public class DashboardController {
	
	private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private CampaignService campaignService;
	
	/**
	 * private dashboard
	 * 
	 */	
	@RequestMapping(value = {"/","/dashboard"}, method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model, Principal principal, @PageableDefaults(pageNumber = 0, value = 4,  sort = "createDate", sortDir = Sort.Direction.DESC) Pageable pageable) {
		logger.info("Dashboard landing page");
		Administrator administrator = administratorService.findByUsername(principal.getName());	
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());	
		Page<CampaignAdministrator> myCampaignAdministratorList = administratorService.myCampaigns(principal.getName(), pageable);
		List<Campaign> myCampaignList = new ArrayList<Campaign>();
		if( myCampaignAdministratorList != null ) {
        	for(CampaignAdministrator campaignAdministrator : myCampaignAdministratorList.getContent()) {
        		myCampaignList.add(campaignAdministrator.getCampaign());
        	}
        }
		model.addAttribute("numberOfBrands", myBrandList.size());
		model.addAttribute("numberOfCampaigns", myCampaignList.size());
		model.addAttribute("numberOfUsers", 0);
		model.addAttribute("myBrandList", myBrandList);
		model.addAttribute("myCampaignList", myCampaignList);
		model.addAttribute("username",administrator.getName());
		model.addAttribute("myCampaignListPageNumber", pageable.getPageNumber());
		if( pageable.getPageNumber() == 0 ) {
			model.addAttribute("myCampaignListFirstPage", true);
		} else {
			model.addAttribute("myCampaignListFirstPage", false);			
		}
		if( ((pageable.getPageNumber() * pageable.getPageSize()) + pageable.getPageSize()) >= myCampaignAdministratorList.getTotalElements() ) {
			model.addAttribute("myCampaignListLastPage", true);
		} else {
			model.addAttribute("myCampaignListLastPage", false);			
		}		
		return "dashboard";
	}
	
	/**
	 * public dashboard
	 * 
	 */	
	@RequestMapping(value = {"/","/publicdashboard"}, method = RequestMethod.GET)
	public String publicdashbard(Locale locale, Model model, Principal principal, @PageableDefaults(pageNumber = 0, value = 4,  sort = "createDate", sortDir = Sort.Direction.DESC) Pageable pageable) {
		logger.info("Public Dashboard landing page");
		Administrator administrator = administratorService.findByUsername(principal.getName());	
		Page<Campaign> publicCampaignList = campaignService.findAll(pageable);
		model.addAttribute("numberOfPublicCampaigns", publicCampaignList.getTotalElements());
		model.addAttribute("numberOfUsers", 0);
		model.addAttribute("publicCampaignList", publicCampaignList.getContent());
		model.addAttribute("username",administrator.getName());
		model.addAttribute("publicCampaignListPageNumber", pageable.getPageNumber());
		if( pageable.getPageNumber() == 0 ) {
			model.addAttribute("publicCampaignListFirstPage", true);
		} else {
			model.addAttribute("publicCampaignListFirstPage", false);			
		}
		if( ((pageable.getPageNumber() * pageable.getPageSize()) + pageable.getPageSize()) >= publicCampaignList.getTotalElements() ) {
			model.addAttribute("publicCampaignListLastPage", true);
		} else {
			model.addAttribute("publicCampaignListLastPage", false);			
		}		
		return "publicdashboard";
	}
}