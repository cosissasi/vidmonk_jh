package com.viddmonk.website;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.service.AdministratorService;
import com.viddmonk.service.BrandService;
import com.viddmonk.service.CampaignService;

@Controller
@SessionAttributes("email")
public class BrandController {
	
	private static final Logger logger = LoggerFactory.getLogger(BrandController.class);
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private BrandService brandService;
	
	
	/**
	 * get all brands per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/brands"}, method = RequestMethod.GET)
	public String getAllBrands(Locale locale, Model model, Principal principal) {
		logger.info("Dashboard landing page");
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "brands";
	}
	
	/**
	 * get all brands per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/brands/add"}, method = RequestMethod.GET)
	public String addBrand(Locale locale, Model model, Principal principal) {
		logger.info("Add A Brand display page");
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "addbrand";
	}
	
	/**
	 * get all brands per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/brands/add/submit"}, method = RequestMethod.POST)
	public String addBrandSubmit(
			Locale locale,
			Model model,
			Principal principal,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "shortDescription", required = false) String shortDescription,
			@RequestParam(value = "longDescription", required = false) String longDescription,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "imageFile", required = false) MultipartFile multipartfile,
			@RequestParam(value = "website", required = false) String website,
			@RequestParam(value = "facebook", required = false) String facebook,
			@RequestParam(value = "twitter", required = false) String twitter) {
		logger.info("Add A Brand page");
		Brand brand = null;
		if( id != null && !"".equals(id) ) {
			brand = brandService.findById(id);
			if( brand == null ) {
				brand = new Brand();				
			}
		} else {
			brand = brandService.findByName(name);
			if( brand == null ) {
				brand = new Brand();				
			}
		}
		brand.setName(name);
		brand.setShortDescription(shortDescription);
		brand.setDescription(longDescription);
		brand.setEmail(email);
		brand.setWebsite(website);
		brand.setFacebook(facebook);
		brand.setTwitter(twitter);
		administratorService.saveBrand(brand, principal.getName(),multipartfile);
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		return "brands";
	}
	
	/**
	 * get all brands per user 
	 * 
	 */	
	@RequestMapping(value = {"/secure/brands/add/campaign/submit"}, method = RequestMethod.POST)
	public String addBrandFromCampaignSubmit(
			Locale locale,
			Model model,
			Principal principal,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "campaignId", required = false) String campaignId,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "shortDescription", required = false) String shortDescription,
			@RequestParam(value = "imageFile", required = false) MultipartFile multipartfile,
			@RequestParam(value = "website", required = false) String website,
			@RequestParam(value = "facebook", required = false) String facebook,
			@RequestParam(value = "twitter", required = false) String twitter) {
		logger.info("Add A Brand page");
		Brand brand = null;
		if( id != null && !"".equals(id) ) {
			brand = brandService.findById(id);
			if( brand == null ) {
				brand = new Brand();				
			}
		} else {
			brand = brandService.findByName(name);
			if( brand == null ) {
				brand = new Brand();				
			}
		}
		brand.setName(name);
		brand.setShortDescription(shortDescription);
		brand.setWebsite(website);
		brand.setFacebook(facebook);
		brand.setTwitter(twitter);
		Brand newbrand = administratorService.saveBrand(brand, principal.getName(),multipartfile);
		List<Brand> myBrandList = administratorService.myBrands(principal.getName());
		model.addAttribute("myBrandList", myBrandList);
		if( campaignId == null || "".equals(campaignId)) {
			model.addAttribute("newbrandId", newbrand.getId());
			return "redirect:" + "/secure/campaigns/add";			
		}
		Campaign campaign = campaignService.findById(campaignId);
		campaign.setBrand(newbrand);
		campaignService.save(campaign);
		return "redirect:" + "/secure/campaigns/update/" + campaignId;
	}
}