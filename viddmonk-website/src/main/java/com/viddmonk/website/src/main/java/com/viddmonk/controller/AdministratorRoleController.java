/**
 * Copyright Viddmonk 2013
 *  
 */
package com.viddmonk.controller;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viddmonk.domain.AdministratorRole;
import com.viddmonk.service.AdministratorRoleService;

/**
 * User Controller
 * @author rvadlam
 *
 */
@Controller
@RequestMapping("/administrator/role/")
public class AdministratorRoleController extends BaseController {
	
	@Autowired
	private AdministratorRoleService administratorRoleService;

    @RequestMapping(method = { RequestMethod.GET })
    public @ResponseBody List<AdministratorRole> findByRole() throws ConstraintViolationException, Exception {
    	return administratorRoleService.findAll();
    }

    @RequestMapping(value="save", method = { RequestMethod.POST })
    public @ResponseBody AdministratorRole save(@RequestBody AdministratorRole administratorRole) throws ConstraintViolationException, Exception {
    	return administratorRoleService.save(administratorRole);
    }
}