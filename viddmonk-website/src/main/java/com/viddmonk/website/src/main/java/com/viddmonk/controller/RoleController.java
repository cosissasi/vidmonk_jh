/**
 * Copyright Viddmonk 2013
 *  
 */
package com.viddmonk.controller;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viddmonk.domain.Role;
import com.viddmonk.service.RoleService;

/**
 * Role Controller
 * @author rvadlam
 *
 */
@Controller
@RequestMapping("/users/role/")
public class RoleController extends BaseController {
	
	@Autowired
	private RoleService roleService;

    @RequestMapping(method = { RequestMethod.GET })
    public @ResponseBody List<Role> findByRole() throws ConstraintViolationException, Exception {
    	return roleService.findAll();
    }

    @RequestMapping(value="save", method = { RequestMethod.POST })
    public @ResponseBody Role save(@RequestBody Role role) throws ConstraintViolationException, Exception {
    	return roleService.save(role);
    }
}