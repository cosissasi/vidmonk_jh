package com.viddmonk.website.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.viddmonk.domain.Administrator;
import com.viddmonk.service.AdministratorService;

/**
 * Login Failure Handler
 * @author rvadlam
 *
 */
public class LoginFailureHandler implements AuthenticationFailureHandler {
	
	private String defaultFailureUrl;
	
	@Autowired
	private AdministratorService administratorService;

	/**
     * Called when an authentication attempt fails.
     * @param request the request during which the authentication attempt occurred.
     * @param response the response.
     * @param exception the exception which was thrown to reject the authentication request.
     */
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		@SuppressWarnings("deprecation")
		String username = (String) request.getSession().getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY);
		Administrator adminUser = null;
		try {
			adminUser = administratorService.findByUsername(username);
		} catch (Exception e1) {
		}	
		
		if( adminUser != null ) {
			adminUser.setNumberOfFailedAttempts(adminUser.getNumberOfFailedAttempts()+1);
			administratorService.save(adminUser);
		}
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + defaultFailureUrl));		
    }

	/**
	 * Get Default Failure Url
	 * @return the defaultFailureUrl
	 * 
	 */
	public String getDefaultFailureUrl() {
		return defaultFailureUrl;
	}

	/**
	 * Set Default Failure Url
	 * @param defaultFailureUrl the defaultFailureUrl to set
	 * 
	 */
	public void setDefaultFailureUrl(String defaultFailureUrl) {
		this.defaultFailureUrl = defaultFailureUrl;
	}
}