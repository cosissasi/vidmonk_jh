/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.util;

/**
 * Error Util Class
 * @author rvadlam
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.viddmonk.view.ErrorType;
import com.viddmonk.view.ResponseError;

/**
 * Error Util class, builds Error response objects
 *
 */
public class ErrorUtil {
	
	/**
	 * build constraint error
	 * @param constraintViolationException	ConstraintViolationException
	 * @param errorType	ErrorType
	 * @return	Map<String, List<ResponseError>>
	 * 
	 */
	public static Map<String, List<ResponseError>> buildConstraintError(ConstraintViolationException constraintViolationException, ErrorType errorType) {
		Map<String, List<ResponseError>> resultMap = new HashMap<String, List<ResponseError>>();
		if( constraintViolationException != null ) {
			List<ResponseError> responseErrorList = new ArrayList<ResponseError>();
			for(ConstraintViolation<?> constraintViolation : constraintViolationException.getConstraintViolations() ) {
				ResponseError responseError = new ResponseError();
				responseError.setErrorType(errorType);
				responseError.setErrorMessage(constraintViolation.getMessage());
				responseError.setErrorProperty(constraintViolation.getPropertyPath().toString());
				responseErrorList.add(responseError);
			}			
			resultMap.put("errorList", responseErrorList);
		}
		return resultMap;	
	}
	
	/**
	 * build severe error
	 * @param constraintViolationException	ConstraintViolationException
	 * @param errorType	ErrorType
	 * @return	Map<String, List<ResponseError>>
	 * 
	 */
	public static Map<String, List<ResponseError>> buildSevereError(Exception exception, ErrorType errorType) {
		Map<String, List<ResponseError>> resultMap = new HashMap<String, List<ResponseError>>();
		if( exception != null ) {
			List<ResponseError> responseErrorList = new ArrayList<ResponseError>();
			ResponseError responseError = new ResponseError();
			responseError.setErrorType(errorType);
			responseError.setErrorMessage(exception.getMessage());
			responseError.setStacktrace(Arrays.toString(exception.getStackTrace()));
			responseErrorList.add(responseError);			
			resultMap.put("errorList", responseErrorList);
		}
		return resultMap;
	}
}
