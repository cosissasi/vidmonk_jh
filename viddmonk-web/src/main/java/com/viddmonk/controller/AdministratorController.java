/**
 * Copyright Viddmonk 2013
 *  
 */
package com.viddmonk.controller;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viddmonk.domain.Administrator;
import com.viddmonk.service.AdministratorService;

/**
 * Administrator Controller
 * @author rvadlam
 *
 */
@Controller
@RequestMapping("/administrators/")
public class AdministratorController extends BaseController {
	
	@Autowired
	private AdministratorService administratorService;

    @RequestMapping(value="registerAdministrator", method = { RequestMethod.POST })
    public @ResponseBody Administrator registerAdministrator(@RequestBody Administrator administrator, @PathVariable String role) throws ConstraintViolationException, Exception {
    	return administratorService.registerAdministrator(administrator);
    }

    @RequestMapping(value="passwordReset", method = { RequestMethod.POST })
    public @ResponseBody Administrator passwordReset(@RequestBody Administrator administrator) throws ConstraintViolationException, Exception {
    	return administratorService.passwordReset(administrator);
    }

    @RequestMapping(value="changePassword", method = { RequestMethod.POST })
    public @ResponseBody Administrator changePassword(@RequestBody Administrator administrator) throws ConstraintViolationException, Exception {
    	return administratorService.changePassword(administrator);
    }
}