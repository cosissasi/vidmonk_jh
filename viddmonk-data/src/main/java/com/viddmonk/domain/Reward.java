package com.viddmonk.domain;
/**
 * Copyright Viddmonk 2013
 * 
 */

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Reward domain class which represents Reward Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "rewards")
public class Reward implements Serializable {

	private static final long serialVersionUID = -5274546266253881834L;

	@Id
	private String id;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String name;
	
	@Size(max=256)	
    private String shortDescription;	
	
	@Size(max=2000)	
    private String description;	
	
    private Long value;	
	
    private Long quantityAllowed;	
	
    private Long quantityUsed;	

	@DBRef
	private RewardType rewardType;
	
	@DBRef
	private Set<Media> rewardImageList = new HashSet<Media>();

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get name
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * @param name the name to set
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get shortDescription
	 * @return the shortDescription
	 *
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * set shortDescription
	 * @param shortDescription the shortDescription to set
	 *
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * get description
	 * @return the description
	 *
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * set description
	 * @param description the description to set
	 *
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * get value
	 * @return the value
	 *
	 */
	public Long getValue() {
		return value;
	}

	/**
	 * set value
	 * @param value the value to set
	 *
	 */
	public void setValue(Long value) {
		this.value = value;
	}

	/**
	 * get quantityAllowed
	 * @return the quantityAllowed
	 *
	 */
	public Long getQuantityAllowed() {
		return quantityAllowed;
	}

	/**
	 * set quantityAllowed
	 * @param quantityAllowed the quantityAllowed to set
	 *
	 */
	public void setQuantityAllowed(Long quantityAllowed) {
		this.quantityAllowed = quantityAllowed;
	}

	/**
	 * get quantityUsed
	 * @return the quantityUsed
	 *
	 */
	public Long getQuantityUsed() {
		return quantityUsed;
	}

	/**
	 * set quantityUsed
	 * @param quantityUsed the quantityUsed to set
	 *
	 */
	public void setQuantityUsed(Long quantityUsed) {
		this.quantityUsed = quantityUsed;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Reward [id=" + id + ", name=" + name + ", shortDescription="
				+ shortDescription + ", description=" + description
				+ ", value=" + value + ", quantityAllowed=" + quantityAllowed
				+ ", quantityUsed=" + quantityUsed + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((quantityAllowed == null) ? 0 : quantityAllowed.hashCode());
		result = prime * result
				+ ((quantityUsed == null) ? 0 : quantityUsed.hashCode());
		result = prime
				* result
				+ ((shortDescription == null) ? 0 : shortDescription.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
}