package com.viddmonk.domain;

/**
 * Account State enum
 * @author rvadlam
 *
 */
public enum AccountState {
	 UnVerfied
	,Active
	,Locked
	,InActive
	,Blocked
	,PasswordReset

}
