/**
 * Copyright ViddMonk 2013
 * 
 */
package com.viddmonk.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Role domain object which represents Role table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "roles")
public class Role implements Serializable {

	private static final long serialVersionUID = -5162921000072187285L;

	@Id
	private String id;
	
	@Size(max=128)
	private String role;
	
	@Size(max=128)
	private String description;

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get role
	 * @return the role
	 *
	 */
	public String getRole() {
		return role;
	}

	/**
	 * set role
	 * @param role the role to set
	 *
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * get description
	 * @return the description
	 *
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * set description
	 * @param description the description to set
	 *
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}