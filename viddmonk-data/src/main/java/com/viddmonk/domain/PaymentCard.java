package com.viddmonk.domain;

/**
 * Copyright Viddmonk 2013
 * 
 */

import java.io.Serializable;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Payment Card domain class which represents Payment Card Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "paymentcards")
public class PaymentCard extends Auditable implements Serializable {

	private static final long serialVersionUID = 3979381787825469382L;

	@Id
	private String id;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String creditCardNumber;
	
	@Size(max=256)	
	private String cardholderName;	
	
	@Size(max=256)	
	private String securityCode;	
		
	@Size(max=2000)	
    private String address;	
	
	@Size(max=256)	
	private String nickname;

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get creditCardNumber
	 * @return the credirCardNumber
	 *
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	/**
	 * set credirCardNumber
	 * @param credirCardNumber the credirCardNumber to set
	 *
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	/**
	 * get cardholderName
	 * @return the cardholderName
	 *
	 */
	public String getCardholderName() {
		return cardholderName;
	}

	/**
	 * set cardholderName
	 * @param cardholderName the cardholderName to set
	 *
	 */
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}

	/**
	 * get securityCode
	 * @return the securityCode
	 *
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * set securityCode
	 * @param securityCode the securityCode to set
	 *
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * get address
	 * @return the address
	 *
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * set address
	 * @param address the address to set
	 *
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * get nickname
	 * @return the nickname
	 *
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * set nickname
	 * @param nickname the nickname to set
	 *
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((cardholderName == null) ? 0 : cardholderName.hashCode());
		result = prime
				* result
				+ ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result
				+ ((securityCode == null) ? 0 : securityCode.hashCode());
		return result;
	}
}