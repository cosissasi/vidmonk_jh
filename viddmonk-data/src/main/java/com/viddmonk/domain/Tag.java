package com.viddmonk.domain;

/**
 * Copyright Viddmonk 2013
 * 
 */

import java.io.Serializable;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Tag domain class which represents Tag Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "tags")
public class Tag implements Serializable {

	private static final long serialVersionUID = 2289365524091695155L;

	@Id
	private String id;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String name;
	
	@DBRef
	private TagType tagType;

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get name
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * @param name the name to set
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get tagType
	 * @return the tagType
	 *
	 */
	public TagType getTagType() {
		return tagType;
	}

	/**
	 * set tagType
	 * @param tagType the tagType to set
	 *
	 */
	public void setTagType(TagType tagType) {
		this.tagType = tagType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + ", tagType=" + tagType
				+ "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tagType == null) ? 0 : tagType.hashCode());
		return result;
	}
}