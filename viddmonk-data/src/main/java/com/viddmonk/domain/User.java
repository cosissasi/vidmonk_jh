/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

/**
 * User domain object which represents User table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 1355011406252270141L;

	@Id
	private String id;
	
	@Size(max=128)
	private String username;
	
	@Size(max=128)	
	private String password;
	
	@Size(max=128)	
	private String oneTimePassword;
	
	@NotNull
	@NotEmpty
	@Email
	private String email;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String name;
	
	private AccountState accountState;
	
	private String mobileNumber;
	
    private String zipCode;
    
    private Boolean acceptTermsAndCondition;
    
    private Boolean smsNotification;
    
    private Boolean emailNotification;
    
    private int numberOfFailedAttempts;
    
    private Collection<? extends GrantedAuthority> authorities;

	@DBRef
	private Role role;
	
	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get username
	 * @return the username
	 *
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * set username
	 * @param username the username to set
	 *
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * get password
	 * @return the password
	 *
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set password
	 * @param password the password to set
	 *
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * get email
	 * @return the email
	 *
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * set email
	 * @param email the email to set
	 *
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * get mobileNumber
	 * @return the mobileNumber
	 *
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * set mobileNumber
	 * @param mobileNumber the mobileNumber to set
	 *
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * get zipCode
	 * @return the zipCode
	 *
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * set zipCode
	 * @param zipCode the zipCode to set
	 *
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * get smsNotification
	 * @return the smsNotification
	 *
	 */
	public Boolean getSmsNotification() {
		return smsNotification;
	}

	/**
	 * set smsNotification
	 * @param smsNotification the smsNotification to set
	 *
	 */
	public void setSmsNotification(Boolean smsNotification) {
		this.smsNotification = smsNotification;
	}

	/**
	 * get emailNotification
	 * @return the emailNotification
	 *
	 */
	public Boolean getEmailNotification() {
		return emailNotification;
	}

	/**
	 * set emailNotification
	 * @param emailNotification the emailNotification to set
	 *
	 */
	public void setEmailNotification(Boolean emailNotification) {
		this.emailNotification = emailNotification;
	}

	/**
	 * get role
	 * @return the role
	 *
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * set role
	 * @param role the role to set
	 *
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * get oneTimePassword
	 * @return the oneTimePassword
	 *
	 */
	public String getOneTimePassword() {
		return oneTimePassword;
	}

	/**
	 * set oneTimePassword
	 * @param oneTimePassword the oneTimePassword to set
	 *
	 */
	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

	/**
	 * get accountState
	 * @return the accountState
	 *
	 */
	public AccountState getAccountState() {
		return accountState;
	}

	/**
	 * set accountState
	 * @param accountState the accountState to set
	 *
	 */
	public void setAccountState(AccountState accountState) {
		this.accountState = accountState;
	}

	/**
	 * get numberOfFailedAttempts
	 * @return the numberOfFailedAttempts
	 *
	 */
	public int getNumberOfFailedAttempts() {
		return numberOfFailedAttempts;
	}

	/**
	 * set numberOfFailedAttempts
	 * @param numberOfFailedAttempts the numberOfFailedAttempts to set
	 *
	 */
	public void setNumberOfFailedAttempts(int numberOfFailedAttempts) {
		this.numberOfFailedAttempts = numberOfFailedAttempts;
	}

	/**
	 * get name
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * @param name the name to set
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get acceptTermsAndCondition
	 * @return the acceptTermsAndCondition
	 *
	 */
	public Boolean getAcceptTermsAndCondition() {
		return acceptTermsAndCondition;
	}

	/**
	 * set acceptTermsAndCondition
	 * @param acceptTermsAndCondition the acceptTermsAndCondition to set
	 *
	 */
	public void setAcceptTermsAndCondition(Boolean acceptTermsAndCondition) {
		this.acceptTermsAndCondition = acceptTermsAndCondition;
	}

	/**
	 * get authorities
	 * @return the authorities
	 *
	 */
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/**
	 * set authorities
	 * @param authorities the authorities to set
	 *
	 */
	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	
}