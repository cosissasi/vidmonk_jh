/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Administrator domain object which represents Adminstrator table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "administrators")
public class Administrator implements Serializable {

	private static final long serialVersionUID = -8951771549634465119L;

	@Id
	private String id;
	
	@Size(max=128)
	private String username;
	
	@Size(max=128)	
	private String password;
	
	@NotNull
	@NotEmpty
	@Email
	private String email;
		
	@Size(max=128)	
	private String name;
	
    private String zipCode;

	@DBRef
	private AdministratorRole role;
	
	private AccountState accountState;
    
    private int numberOfFailedAttempts;
	
	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get username
	 * @return the username
	 *
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * set username
	 * @param username the username to set
	 *
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * get password
	 * @return the password
	 *
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set password
	 * @param password the password to set
	 *
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * get email
	 * @return the email
	 *
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * set email
	 * @param email the email to set
	 *
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * get name
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * @param name the name to set
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get role
	 * @return the role
	 *
	 */
	public AdministratorRole getRole() {
		return role;
	}

	/**
	 * set role
	 * @param role the role to set
	 *
	 */
	public void setRole(AdministratorRole role) {
		this.role = role;
	}

	/**
	 * get accountState
	 * @return the accountState
	 *
	 */
	public AccountState getAccountState() {
		return accountState;
	}

	/**
	 * set accountState
	 * @param accountState the accountState to set
	 *
	 */
	public void setAccountState(AccountState accountState) {
		this.accountState = accountState;
	}

	/**
	 * get numberOfFailedAttempts
	 * @return the numberOfFailedAttempts
	 *
	 */
	public int getNumberOfFailedAttempts() {
		return numberOfFailedAttempts;
	}

	/**
	 * set numberOfFailedAttempts
	 * @param numberOfFailedAttempts the numberOfFailedAttempts to set
	 *
	 */
	public void setNumberOfFailedAttempts(int numberOfFailedAttempts) {
		this.numberOfFailedAttempts = numberOfFailedAttempts;
	}

	/**
	 * get zipCode
	 * @return the zipCode
	 *
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * set zipCode
	 * @param zipCode the zipCode to set
	 *
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}