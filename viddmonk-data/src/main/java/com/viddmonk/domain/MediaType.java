package com.viddmonk.domain;

/**
 * Account State enum
 * @author rvadlam
 *
 */
public enum MediaType {
	 IMAGE
	,AUDIO
	,VIDEO
}
