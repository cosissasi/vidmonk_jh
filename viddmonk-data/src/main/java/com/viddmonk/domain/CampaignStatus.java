package com.viddmonk.domain;

/**
 * Campaign Status State enum
 * @author rvadlam
 *
 */
public enum CampaignStatus {
	 CREATED
	,LIVE
	,CLOSED
}