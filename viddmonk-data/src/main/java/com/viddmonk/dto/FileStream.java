package com.viddmonk.dto;

import java.io.InputStream;

/**
 * File Stream
 * @author rvadlam
 *
 */
public class FileStream {
	private InputStream inputStream;
	private long size;
	
	/**
	 * constructor
	 * @param input	InputStream
	 * @param size	long
	 * 
	 */
	public FileStream(InputStream input, long size) {
		this.inputStream = input;
		this.size = size;
	}

	/**
	 * get input stream
	 * @return	InputStream
	 * 
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * set input stream
	 * @param input	InputStream
	 * 
	 */
	public void setInputStream(InputStream input) {
		this.inputStream = input;
	}

	/**
	 * get size
	 * @return	long
	 * 
	 */
	public long getSize() {
		return size;
	}

	/**
	 * set size
	 * @param size	long
	 * 
	 */
	public void setSize(long size) {
		this.size = size;
	}	
}