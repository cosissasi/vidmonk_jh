package com.viddmonk.dto;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.viddmonk.domain.AccountState;
import com.viddmonk.domain.User;

/**
 * Authentication User Details
 * @author rvadlam
 *
 */
public class AuthenticationUserDetails implements UserDetails {

	private static final long serialVersionUID = 6365750299314187082L;

	private final String username;
    private final String passwordHash;
    private final boolean enabled;
    private HashSet<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
 
    /**
     * constructor
     * @param user	User
     * 
     */
    public AuthenticationUserDetails(User user) {
        this.username = user.getUsername();
        this.passwordHash = user.getPassword();
        this.enabled = user.getAccountState().equals(AccountState.Active);
        this.grantedAuthorities.addAll(user.getAuthorities());
    }
 
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }
 
    @Override
    public String getPassword() {
        return passwordHash;
    }
 
    @Override
    public String getUsername() {
        return username;
    }
 
    @Override
    public boolean isEnabled() {
        return enabled;
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 }