package com.viddmonk.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
 
/**
 * Spring MongoDB configuration file
 * 
 */
@Configuration
public class SpringMongoConfig extends AbstractMongoConfiguration{
 
	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}
	 
		@Bean
		public MongoTemplate mongoTemplate() throws Exception {
			return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
		}
 
	@Override
	protected String getDatabaseName() {
		return "viddmonkdb";
	}
 
	@Override
	@Bean
	public Mongo mongo() throws Exception {
		return new MongoClient("localhost");
	}
 
}
