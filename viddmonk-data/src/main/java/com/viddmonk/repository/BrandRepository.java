/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Brand;

/**
 * Brand Repository
 * @author rvadlam
 *
 */
@Repository
public interface BrandRepository extends MongoRepository<Brand, String>, QueryDslPredicateExecutor<Brand>  {
	
	Brand findById(String id);
	
	Brand findByName(String name);
	
	Page<Brand> findByParentBrand(Brand brand, Pageable pageable);

}
