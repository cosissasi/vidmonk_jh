/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.BrandAdministrator;

/**
 * Brand Administrator Repository
 * @author rvadlam
 *
 */
@Repository
public interface BrandAdministratorRepository extends MongoRepository<BrandAdministrator, String>, QueryDslPredicateExecutor<BrandAdministrator>  {
	
	List<BrandAdministrator> findByBrand(Brand brand);
	
	List<BrandAdministrator> findByAdministrator(Administrator administrator);
	
	BrandAdministrator findByBrandAndAdministrator(Brand brand, Administrator administrator);

}
