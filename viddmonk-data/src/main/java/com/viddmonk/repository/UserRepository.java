/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.User;

/**
 * User Repository
 * @author rvadlam
 *
 */
@Repository
public interface UserRepository extends MongoRepository<User, String>, QueryDslPredicateExecutor<User>  {
	
	User findById(String id);
	
	User findByUsername(String username);
}
