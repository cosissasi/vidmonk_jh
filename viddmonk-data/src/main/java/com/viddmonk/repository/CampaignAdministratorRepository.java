/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.CampaignAdministrator;

/**
 * Campaign Administrator Repository
 * @author rvadlam
 *
 */
@Repository
public interface CampaignAdministratorRepository extends MongoRepository<CampaignAdministrator, String>, QueryDslPredicateExecutor<CampaignAdministrator>  {
	
	Page<CampaignAdministrator> findByCampaign(Campaign campaign, Pageable pageable);
	
	Page<CampaignAdministrator> findByAdministrator(Administrator administrator, Pageable pageable);

}
