/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Administrator;

/**
 * Administrator Repository
 * @author rvadlam
 *
 */
@Repository
public interface AdministratorRepository extends MongoRepository<Administrator, String>, QueryDslPredicateExecutor<Administrator>  {
	
	Administrator findByUsername(String username);
	
	Administrator findByEmail(String email);

}
