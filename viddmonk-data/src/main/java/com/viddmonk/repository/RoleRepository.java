/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Role;

/**
 * Role Repository
 * @author rvadlam
 *
 */
@Repository
public interface RoleRepository extends MongoRepository<Role, String>, QueryDslPredicateExecutor<Role>  {

	Role findByRole(String role);
	
}
