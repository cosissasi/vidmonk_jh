package com.viddmonk.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.types.expr.BooleanExpression;
import com.viddmonk.domain.QUser;
import com.viddmonk.domain.User;
import com.viddmonk.repository.UserRepository;

/**
 * User DAO Interface
 *
 */
@Repository
public class UserDAOImpl implements UserDAO {
        
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * find by username
	 * @param	username	Username
	 * @return	User
	 * 
	 */
	public User findByUsername(String username) {
		QUser user = QUser.user;
		BooleanExpression expression = user.username.eq(username);
		return userRepository.findOne(expression);
	}
}