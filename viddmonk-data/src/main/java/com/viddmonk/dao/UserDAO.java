package com.viddmonk.dao;

import com.viddmonk.domain.User;

/**
 * User DAO Interface
 *
 */
/**
test
*/
public interface UserDAO {
	
	public User findByUsername(String username);

}