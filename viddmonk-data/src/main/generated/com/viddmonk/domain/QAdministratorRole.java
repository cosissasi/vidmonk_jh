package com.viddmonk.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAdministratorRole is a Querydsl query type for AdministratorRole
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAdministratorRole extends EntityPathBase<AdministratorRole> {

    private static final long serialVersionUID = 1834850184;

    public static final QAdministratorRole administratorRole = new QAdministratorRole("administratorRole");

    public final StringPath description = createString("description");

    public final StringPath id = createString("id");

    public final StringPath role = createString("role");

    public QAdministratorRole(String variable) {
        super(AdministratorRole.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QAdministratorRole(Path<? extends AdministratorRole> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    public QAdministratorRole(PathMetadata<?> metadata) {
        super(AdministratorRole.class, metadata);
    }

}

