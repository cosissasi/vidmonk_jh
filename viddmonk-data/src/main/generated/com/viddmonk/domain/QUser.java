package com.viddmonk.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -1173621498;

    private static final PathInits INITS = PathInits.DIRECT;

    public static final QUser user = new QUser("user");

    public final BooleanPath acceptTermsAndCondition = createBoolean("acceptTermsAndCondition");

    public final EnumPath<AccountState> accountState = createEnum("accountState", AccountState.class);

    public final CollectionPath<org.springframework.security.core.GrantedAuthority, org.springframework.security.core.QGrantedAuthority> authorities = this.<org.springframework.security.core.GrantedAuthority, org.springframework.security.core.QGrantedAuthority>createCollection("authorities", org.springframework.security.core.GrantedAuthority.class, org.springframework.security.core.QGrantedAuthority.class, PathInits.DIRECT);

    public final StringPath email = createString("email");

    public final BooleanPath emailNotification = createBoolean("emailNotification");

    public final StringPath id = createString("id");

    public final StringPath mobileNumber = createString("mobileNumber");

    public final StringPath name = createString("name");

    public final NumberPath<Integer> numberOfFailedAttempts = createNumber("numberOfFailedAttempts", Integer.class);

    public final StringPath oneTimePassword = createString("oneTimePassword");

    public final StringPath password = createString("password");

    public final QRole role;

    public final BooleanPath smsNotification = createBoolean("smsNotification");

    public final StringPath username = createString("username");

    public final StringPath zipCode = createString("zipCode");

    public QUser(String variable) {
        this(User.class, forVariable(variable), INITS);
    }

    @SuppressWarnings("all")
    public QUser(Path<? extends User> path) {
        this((Class)path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUser(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUser(PathMetadata<?> metadata, PathInits inits) {
        this(User.class, metadata, inits);
    }

    public QUser(Class<? extends User> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.role = inits.isInitialized("role") ? new QRole(forProperty("role")) : null;
    }

}

