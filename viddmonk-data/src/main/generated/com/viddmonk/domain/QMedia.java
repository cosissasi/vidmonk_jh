package com.viddmonk.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QMedia is a Querydsl query type for Media
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMedia extends EntityPathBase<Media> {

    private static final long serialVersionUID = -2030334455;

    public static final QMedia media = new QMedia("media");

    public final StringPath filename = createString("filename");

    public final NumberPath<Long> fileSize = createNumber("fileSize", Long.class);

    public final StringPath id = createString("id");

    public final NumberPath<Long> length = createNumber("length", Long.class);

    public final EnumPath<MediaStatus> mediaStatus = createEnum("mediaStatus", MediaStatus.class);

    public final EnumPath<MediaType> mediaType = createEnum("mediaType", MediaType.class);

    public final StringPath s3URL = createString("s3URL");

    public final DateTimePath<java.util.Date> uploadedTime = createDateTime("uploadedTime", java.util.Date.class);

    public QMedia(String variable) {
        super(Media.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QMedia(Path<? extends Media> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    public QMedia(PathMetadata<?> metadata) {
        super(Media.class, metadata);
    }

}

